﻿namespace CharacterManager66
{
    partial class DeathForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DeathForm));
            this.CheckResult = new System.Windows.Forms.GroupBox();
            this.CheckHitsResult = new System.Windows.Forms.Label();
            this.CheckProficiencyUsed = new System.Windows.Forms.Label();
            this.D6 = new System.Windows.Forms.PictureBox();
            this.D5 = new System.Windows.Forms.PictureBox();
            this.D4 = new System.Windows.Forms.PictureBox();
            this.D3 = new System.Windows.Forms.PictureBox();
            this.D2 = new System.Windows.Forms.PictureBox();
            this.D1 = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.HealthRemaining = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.DeathPoolSize = new System.Windows.Forms.Label();
            this.DecrDeathPool = new System.Windows.Forms.Button();
            this.IncrDeathPool = new System.Windows.Forms.Button();
            this.DeathRollBtn = new System.Windows.Forms.Button();
            this.CheckTime = new System.Windows.Forms.Label();
            this.CheckResult.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.D6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.D5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.D4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.D3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.D2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.D1)).BeginInit();
            this.SuspendLayout();
            // 
            // CheckResult
            // 
            this.CheckResult.Controls.Add(this.CheckHitsResult);
            this.CheckResult.Controls.Add(this.CheckTime);
            this.CheckResult.Controls.Add(this.CheckProficiencyUsed);
            this.CheckResult.Controls.Add(this.D6);
            this.CheckResult.Controls.Add(this.D5);
            this.CheckResult.Controls.Add(this.D4);
            this.CheckResult.Controls.Add(this.D3);
            this.CheckResult.Controls.Add(this.D2);
            this.CheckResult.Controls.Add(this.D1);
            this.CheckResult.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.CheckResult.Location = new System.Drawing.Point(0, 158);
            this.CheckResult.Name = "CheckResult";
            this.CheckResult.Size = new System.Drawing.Size(347, 302);
            this.CheckResult.TabIndex = 2;
            this.CheckResult.TabStop = false;
            this.CheckResult.Text = "Check";
            // 
            // CheckHitsResult
            // 
            this.CheckHitsResult.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.CheckHitsResult.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CheckHitsResult.Location = new System.Drawing.Point(-4, 244);
            this.CheckHitsResult.Name = "CheckHitsResult";
            this.CheckHitsResult.Size = new System.Drawing.Size(347, 25);
            this.CheckHitsResult.TabIndex = 8;
            this.CheckHitsResult.Text = "Rolled 5 Hits, 3 Misses. -3 Health";
            this.CheckHitsResult.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // CheckProficiencyUsed
            // 
            this.CheckProficiencyUsed.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.CheckProficiencyUsed.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CheckProficiencyUsed.Location = new System.Drawing.Point(0, 11);
            this.CheckProficiencyUsed.Name = "CheckProficiencyUsed";
            this.CheckProficiencyUsed.Size = new System.Drawing.Size(347, 21);
            this.CheckProficiencyUsed.TabIndex = 6;
            this.CheckProficiencyUsed.Text = "DEATH ROLL";
            this.CheckProficiencyUsed.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // D6
            // 
            this.D6.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.D6.Image = ((System.Drawing.Image)(resources.GetObject("D6.Image")));
            this.D6.InitialImage = ((System.Drawing.Image)(resources.GetObject("D6.InitialImage")));
            this.D6.Location = new System.Drawing.Point(230, 141);
            this.D6.MaximumSize = new System.Drawing.Size(100, 100);
            this.D6.MinimumSize = new System.Drawing.Size(100, 100);
            this.D6.Name = "D6";
            this.D6.Size = new System.Drawing.Size(100, 100);
            this.D6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.D6.TabIndex = 5;
            this.D6.TabStop = false;
            // 
            // D5
            // 
            this.D5.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.D5.Image = ((System.Drawing.Image)(resources.GetObject("D5.Image")));
            this.D5.InitialImage = ((System.Drawing.Image)(resources.GetObject("D5.InitialImage")));
            this.D5.Location = new System.Drawing.Point(124, 141);
            this.D5.MaximumSize = new System.Drawing.Size(100, 100);
            this.D5.MinimumSize = new System.Drawing.Size(100, 100);
            this.D5.Name = "D5";
            this.D5.Size = new System.Drawing.Size(100, 100);
            this.D5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.D5.TabIndex = 4;
            this.D5.TabStop = false;
            // 
            // D4
            // 
            this.D4.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.D4.Image = ((System.Drawing.Image)(resources.GetObject("D4.Image")));
            this.D4.InitialImage = ((System.Drawing.Image)(resources.GetObject("D4.InitialImage")));
            this.D4.Location = new System.Drawing.Point(18, 141);
            this.D4.MaximumSize = new System.Drawing.Size(100, 100);
            this.D4.MinimumSize = new System.Drawing.Size(100, 100);
            this.D4.Name = "D4";
            this.D4.Size = new System.Drawing.Size(100, 100);
            this.D4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.D4.TabIndex = 3;
            this.D4.TabStop = false;
            // 
            // D3
            // 
            this.D3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.D3.Image = ((System.Drawing.Image)(resources.GetObject("D3.Image")));
            this.D3.InitialImage = ((System.Drawing.Image)(resources.GetObject("D3.InitialImage")));
            this.D3.Location = new System.Drawing.Point(230, 35);
            this.D3.MaximumSize = new System.Drawing.Size(100, 100);
            this.D3.MinimumSize = new System.Drawing.Size(100, 100);
            this.D3.Name = "D3";
            this.D3.Size = new System.Drawing.Size(100, 100);
            this.D3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.D3.TabIndex = 2;
            this.D3.TabStop = false;
            // 
            // D2
            // 
            this.D2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.D2.Image = ((System.Drawing.Image)(resources.GetObject("D2.Image")));
            this.D2.InitialImage = ((System.Drawing.Image)(resources.GetObject("D2.InitialImage")));
            this.D2.Location = new System.Drawing.Point(124, 35);
            this.D2.MaximumSize = new System.Drawing.Size(100, 100);
            this.D2.MinimumSize = new System.Drawing.Size(100, 100);
            this.D2.Name = "D2";
            this.D2.Size = new System.Drawing.Size(100, 100);
            this.D2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.D2.TabIndex = 1;
            this.D2.TabStop = false;
            // 
            // D1
            // 
            this.D1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.D1.Image = ((System.Drawing.Image)(resources.GetObject("D1.Image")));
            this.D1.InitialImage = ((System.Drawing.Image)(resources.GetObject("D1.InitialImage")));
            this.D1.Location = new System.Drawing.Point(18, 35);
            this.D1.MaximumSize = new System.Drawing.Size(100, 100);
            this.D1.MinimumSize = new System.Drawing.Size(100, 100);
            this.D1.Name = "D1";
            this.D1.Size = new System.Drawing.Size(100, 100);
            this.D1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.D1.TabIndex = 0;
            this.D1.TabStop = false;
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(0, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(347, 39);
            this.label1.TabIndex = 9;
            this.label1.Text = "DEATH";
            this.label1.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // HealthRemaining
            // 
            this.HealthRemaining.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.HealthRemaining.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.HealthRemaining.Location = new System.Drawing.Point(0, 46);
            this.HealthRemaining.Name = "HealthRemaining";
            this.HealthRemaining.Size = new System.Drawing.Size(347, 39);
            this.HealthRemaining.TabIndex = 10;
            this.HealthRemaining.Text = "Health Remaining: 3";
            this.HealthRemaining.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(0, 75);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(347, 39);
            this.label2.TabIndex = 11;
            this.label2.Text = "Death Roll Pool:";
            this.label2.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // DeathPoolSize
            // 
            this.DeathPoolSize.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DeathPoolSize.Location = new System.Drawing.Point(131, 100);
            this.DeathPoolSize.Name = "DeathPoolSize";
            this.DeathPoolSize.Size = new System.Drawing.Size(82, 23);
            this.DeathPoolSize.TabIndex = 25;
            this.DeathPoolSize.Text = "5";
            this.DeathPoolSize.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // DecrDeathPool
            // 
            this.DecrDeathPool.Location = new System.Drawing.Point(97, 101);
            this.DecrDeathPool.Name = "DecrDeathPool";
            this.DecrDeathPool.Size = new System.Drawing.Size(25, 23);
            this.DecrDeathPool.TabIndex = 24;
            this.DecrDeathPool.Text = "-";
            this.DecrDeathPool.UseVisualStyleBackColor = true;
            this.DecrDeathPool.Click += new System.EventHandler(this.DecrDeathPool_Click);
            // 
            // IncrDeathPool
            // 
            this.IncrDeathPool.Location = new System.Drawing.Point(221, 101);
            this.IncrDeathPool.Name = "IncrDeathPool";
            this.IncrDeathPool.Size = new System.Drawing.Size(25, 23);
            this.IncrDeathPool.TabIndex = 23;
            this.IncrDeathPool.Text = "+";
            this.IncrDeathPool.UseVisualStyleBackColor = true;
            this.IncrDeathPool.Click += new System.EventHandler(this.IncrDeathPool_Click);
            // 
            // DeathRollBtn
            // 
            this.DeathRollBtn.Location = new System.Drawing.Point(136, 126);
            this.DeathRollBtn.Name = "DeathRollBtn";
            this.DeathRollBtn.Size = new System.Drawing.Size(75, 23);
            this.DeathRollBtn.TabIndex = 26;
            this.DeathRollBtn.Text = "Roll";
            this.DeathRollBtn.UseVisualStyleBackColor = true;
            this.DeathRollBtn.Click += new System.EventHandler(this.DeathRollBtn_Click);
            // 
            // CheckTime
            // 
            this.CheckTime.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.CheckTime.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CheckTime.Location = new System.Drawing.Point(0, 277);
            this.CheckTime.Name = "CheckTime";
            this.CheckTime.Size = new System.Drawing.Size(347, 25);
            this.CheckTime.TabIndex = 7;
            this.CheckTime.Text = "Check made at: 00:00";
            this.CheckTime.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // DeathForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(347, 460);
            this.Controls.Add(this.DeathRollBtn);
            this.Controls.Add(this.DeathPoolSize);
            this.Controls.Add(this.DecrDeathPool);
            this.Controls.Add(this.IncrDeathPool);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.HealthRemaining);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.CheckResult);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "DeathForm";
            this.Text = "DeathForm";
            this.TopMost = true;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.DeathForm_FormClosing);
            this.CheckResult.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.D6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.D5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.D4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.D3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.D2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.D1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox CheckResult;
        private System.Windows.Forms.Label CheckHitsResult;
        private System.Windows.Forms.Label CheckProficiencyUsed;
        private System.Windows.Forms.PictureBox D6;
        private System.Windows.Forms.PictureBox D5;
        private System.Windows.Forms.PictureBox D4;
        private System.Windows.Forms.PictureBox D3;
        private System.Windows.Forms.PictureBox D2;
        private System.Windows.Forms.PictureBox D1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label HealthRemaining;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label DeathPoolSize;
        private System.Windows.Forms.Button DecrDeathPool;
        private System.Windows.Forms.Button IncrDeathPool;
        private System.Windows.Forms.Button DeathRollBtn;
        private System.Windows.Forms.Label CheckTime;
    }
}