﻿namespace CharacterManager66
{
    partial class BioForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(BioForm));
            this.AgeBox = new System.Windows.Forms.TextBox();
            this.MovementBox = new System.Windows.Forms.TextBox();
            this.BackgroundBox = new System.Windows.Forms.TextBox();
            this.AlignmentBox = new System.Windows.Forms.ComboBox();
            this.PersonalityTraitsBox = new System.Windows.Forms.TextBox();
            this.IdealsBox = new System.Windows.Forms.TextBox();
            this.FlawsBox = new System.Windows.Forms.TextBox();
            this.BioBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // AgeBox
            // 
            this.AgeBox.Location = new System.Drawing.Point(104, 13);
            this.AgeBox.Name = "AgeBox";
            this.AgeBox.Size = new System.Drawing.Size(74, 20);
            this.AgeBox.TabIndex = 0;
            this.AgeBox.TextChanged += new System.EventHandler(this.AgeBox_TextChanged);
            // 
            // MovementBox
            // 
            this.MovementBox.Location = new System.Drawing.Point(247, 13);
            this.MovementBox.Name = "MovementBox";
            this.MovementBox.Size = new System.Drawing.Size(81, 20);
            this.MovementBox.TabIndex = 1;
            this.MovementBox.TextChanged += new System.EventHandler(this.MovementBox_TextChanged);
            // 
            // BackgroundBox
            // 
            this.BackgroundBox.Location = new System.Drawing.Point(104, 66);
            this.BackgroundBox.Name = "BackgroundBox";
            this.BackgroundBox.Size = new System.Drawing.Size(224, 20);
            this.BackgroundBox.TabIndex = 2;
            this.BackgroundBox.TextChanged += new System.EventHandler(this.BackgroundBox_TextChanged);
            // 
            // AlignmentBox
            // 
            this.AlignmentBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.AlignmentBox.FormattingEnabled = true;
            this.AlignmentBox.Location = new System.Drawing.Point(104, 39);
            this.AlignmentBox.Name = "AlignmentBox";
            this.AlignmentBox.Size = new System.Drawing.Size(224, 21);
            this.AlignmentBox.TabIndex = 3;
            this.AlignmentBox.SelectedIndexChanged += new System.EventHandler(this.AlignmentBox_SelectedIndexChanged);
            // 
            // PersonalityTraitsBox
            // 
            this.PersonalityTraitsBox.AcceptsReturn = true;
            this.PersonalityTraitsBox.Location = new System.Drawing.Point(104, 92);
            this.PersonalityTraitsBox.Multiline = true;
            this.PersonalityTraitsBox.Name = "PersonalityTraitsBox";
            this.PersonalityTraitsBox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.PersonalityTraitsBox.Size = new System.Drawing.Size(224, 48);
            this.PersonalityTraitsBox.TabIndex = 4;
            this.PersonalityTraitsBox.TextChanged += new System.EventHandler(this.PersonalityTraitsBox_TextChanged);
            // 
            // IdealsBox
            // 
            this.IdealsBox.AcceptsReturn = true;
            this.IdealsBox.Location = new System.Drawing.Point(104, 146);
            this.IdealsBox.Multiline = true;
            this.IdealsBox.Name = "IdealsBox";
            this.IdealsBox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.IdealsBox.Size = new System.Drawing.Size(224, 48);
            this.IdealsBox.TabIndex = 5;
            this.IdealsBox.TextChanged += new System.EventHandler(this.IdealsBox_TextChanged);
            // 
            // FlawsBox
            // 
            this.FlawsBox.AcceptsReturn = true;
            this.FlawsBox.Location = new System.Drawing.Point(104, 200);
            this.FlawsBox.Multiline = true;
            this.FlawsBox.Name = "FlawsBox";
            this.FlawsBox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.FlawsBox.Size = new System.Drawing.Size(224, 48);
            this.FlawsBox.TabIndex = 6;
            this.FlawsBox.TextChanged += new System.EventHandler(this.FlawsBox_TextChanged);
            // 
            // BioBox
            // 
            this.BioBox.AcceptsReturn = true;
            this.BioBox.Location = new System.Drawing.Point(12, 279);
            this.BioBox.Multiline = true;
            this.BioBox.Name = "BioBox";
            this.BioBox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.BioBox.Size = new System.Drawing.Size(316, 209);
            this.BioBox.TabIndex = 7;
            this.BioBox.TextChanged += new System.EventHandler(this.BioBox_TextChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(29, 13);
            this.label1.TabIndex = 8;
            this.label1.Text = "Age:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 42);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(56, 13);
            this.label2.TabIndex = 9;
            this.label2.Text = "Alignment:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(184, 16);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(60, 13);
            this.label3.TabIndex = 10;
            this.label3.Text = "Movement:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 69);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(68, 13);
            this.label4.TabIndex = 11;
            this.label4.Text = "Background:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(12, 95);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(90, 13);
            this.label5.TabIndex = 12;
            this.label5.Text = "Personality Traits:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(12, 149);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(38, 13);
            this.label6.TabIndex = 13;
            this.label6.Text = "Ideals:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(12, 203);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(37, 13);
            this.label7.TabIndex = 14;
            this.label7.Text = "Flaws:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(12, 263);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(25, 13);
            this.label8.TabIndex = 15;
            this.label8.Text = "Bio:";
            // 
            // BioForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(340, 500);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.BioBox);
            this.Controls.Add(this.FlawsBox);
            this.Controls.Add(this.IdealsBox);
            this.Controls.Add(this.PersonalityTraitsBox);
            this.Controls.Add(this.AlignmentBox);
            this.Controls.Add(this.BackgroundBox);
            this.Controls.Add(this.MovementBox);
            this.Controls.Add(this.AgeBox);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "BioForm";
            this.Text = "Bio";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox AgeBox;
        private System.Windows.Forms.TextBox MovementBox;
        private System.Windows.Forms.TextBox BackgroundBox;
        private System.Windows.Forms.ComboBox AlignmentBox;
        private System.Windows.Forms.TextBox PersonalityTraitsBox;
        private System.Windows.Forms.TextBox IdealsBox;
        private System.Windows.Forms.TextBox FlawsBox;
        private System.Windows.Forms.TextBox BioBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
    }
}