﻿namespace CharacterManager66
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            System.Windows.Forms.ListViewGroup listViewGroup1 = new System.Windows.Forms.ListViewGroup("Equipped", System.Windows.Forms.HorizontalAlignment.Left);
            System.Windows.Forms.ListViewGroup listViewGroup2 = new System.Windows.Forms.ListViewGroup("Carried", System.Windows.Forms.HorizontalAlignment.Left);
            System.Windows.Forms.ListViewItem listViewItem1 = new System.Windows.Forms.ListViewItem("Equ");
            System.Windows.Forms.ListViewItem listViewItem2 = new System.Windows.Forms.ListViewItem("Carri");
            System.Windows.Forms.ListViewGroup listViewGroup3 = new System.Windows.Forms.ListViewGroup("Level 1", System.Windows.Forms.HorizontalAlignment.Left);
            System.Windows.Forms.ListViewGroup listViewGroup4 = new System.Windows.Forms.ListViewGroup("Level 2", System.Windows.Forms.HorizontalAlignment.Left);
            System.Windows.Forms.ListViewGroup listViewGroup5 = new System.Windows.Forms.ListViewGroup("Level 3", System.Windows.Forms.HorizontalAlignment.Left);
            System.Windows.Forms.ListViewGroup listViewGroup6 = new System.Windows.Forms.ListViewGroup("Level 4", System.Windows.Forms.HorizontalAlignment.Left);
            System.Windows.Forms.ListViewGroup listViewGroup7 = new System.Windows.Forms.ListViewGroup("Level 5", System.Windows.Forms.HorizontalAlignment.Left);
            System.Windows.Forms.ListViewGroup listViewGroup8 = new System.Windows.Forms.ListViewGroup("Level 6", System.Windows.Forms.HorizontalAlignment.Left);
            System.Windows.Forms.ListViewItem listViewItem3 = new System.Windows.Forms.ListViewItem("Equ");
            System.Windows.Forms.ListViewItem listViewItem4 = new System.Windows.Forms.ListViewItem("Carri");
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.characterToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.newToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.saveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveAsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.openToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.bioToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.CheckResult = new System.Windows.Forms.GroupBox();
            this.CheckHitsResult = new System.Windows.Forms.Label();
            this.CheckTime = new System.Windows.Forms.Label();
            this.CheckProficiencyUsed = new System.Windows.Forms.Label();
            this.D6 = new System.Windows.Forms.PictureBox();
            this.D5 = new System.Windows.Forms.PictureBox();
            this.D4 = new System.Windows.Forms.PictureBox();
            this.D3 = new System.Windows.Forms.PictureBox();
            this.D2 = new System.Windows.Forms.PictureBox();
            this.D1 = new System.Windows.Forms.PictureBox();
            this.ProficiencyList = new System.Windows.Forms.ComboBox();
            this.AddProficiency = new System.Windows.Forms.Button();
            this.ProficiencyContainer = new System.Windows.Forms.GroupBox();
            this.raceBonus = new System.Windows.Forms.CheckBox();
            this.ProfAdvLabel = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.DecrProfAdv = new System.Windows.Forms.Button();
            this.IncrProfAdv = new System.Windows.Forms.Button();
            this.PointsAvailableLabel = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.ProfDifficultyLabel = new System.Windows.Forms.Label();
            this.ProfLevelLabel = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.DecrProficiency = new System.Windows.Forms.Button();
            this.AdvProficiency = new System.Windows.Forms.Button();
            this.DeleteProficiency = new System.Windows.Forms.Button();
            this.RollCheckButton = new System.Windows.Forms.Button();
            this.SaveFileDialog = new System.Windows.Forms.SaveFileDialog();
            this.OpenFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.NameField = new System.Windows.Forms.TextBox();
            this.RaceField = new System.Windows.Forms.TextBox();
            this.ExpLabel = new System.Windows.Forms.Label();
            this.LowerExp = new System.Windows.Forms.Button();
            this.IncrExp = new System.Windows.Forms.Button();
            this.LevelLabel = new System.Windows.Forms.Label();
            this.LowerLevel = new System.Windows.Forms.Button();
            this.IncrLevel = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.HealthBar = new System.Windows.Forms.ProgressBar();
            this.label4 = new System.Windows.Forms.Label();
            this.CurrentHealthLabel = new System.Windows.Forms.Label();
            this.DecrHealthButton = new System.Windows.Forms.Button();
            this.IncrHealthButton = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            this.MaximumHealthLabel = new System.Windows.Forms.Label();
            this.DecrMaxHealthButton = new System.Windows.Forms.Button();
            this.IncrMaxHealthButton = new System.Windows.Forms.Button();
            this.InventoryControl = new System.Windows.Forms.TabControl();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.SelectedItemName = new System.Windows.Forms.TextBox();
            this.AddItem = new System.Windows.Forms.Button();
            this.RemoveItem = new System.Windows.Forms.Button();
            this.InventoryItemEquipped = new System.Windows.Forms.CheckBox();
            this.InventoryList = new System.Windows.Forms.ListView();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.SpellName = new System.Windows.Forms.TextBox();
            this.NewSpell = new System.Windows.Forms.Button();
            this.DeleteSpell = new System.Windows.Forms.Button();
            this.SpellLevel = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.SpellLevelDecrease = new System.Windows.Forms.Button();
            this.SpellLevelIncrease = new System.Windows.Forms.Button();
            this.ArcaneList = new System.Windows.Forms.ListView();
            this.proficiencyBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.moveBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.proficiencyBindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.proficiencyBindingSource2 = new System.Windows.Forms.BindingSource(this.components);
            this.ExperienceBar = new System.Windows.Forms.ProgressBar();
            this.menuStrip1.SuspendLayout();
            this.CheckResult.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.D6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.D5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.D4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.D3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.D2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.D1)).BeginInit();
            this.ProficiencyContainer.SuspendLayout();
            this.InventoryControl.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.proficiencyBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.moveBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.proficiencyBindingSource1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.proficiencyBindingSource2)).BeginInit();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.characterToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(533, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // characterToolStripMenuItem
            // 
            this.characterToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newToolStripMenuItem,
            this.toolStripSeparator2,
            this.saveToolStripMenuItem,
            this.saveAsToolStripMenuItem,
            this.toolStripSeparator1,
            this.openToolStripMenuItem,
            this.toolStripSeparator3,
            this.bioToolStripMenuItem});
            this.characterToolStripMenuItem.Name = "characterToolStripMenuItem";
            this.characterToolStripMenuItem.Size = new System.Drawing.Size(70, 20);
            this.characterToolStripMenuItem.Text = "Character";
            this.characterToolStripMenuItem.DropDownItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.characterToolStripMenuItem_DropDownItemClicked);
            // 
            // newToolStripMenuItem
            // 
            this.newToolStripMenuItem.Name = "newToolStripMenuItem";
            this.newToolStripMenuItem.ShortcutKeyDisplayString = "";
            this.newToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.N)));
            this.newToolStripMenuItem.Size = new System.Drawing.Size(195, 22);
            this.newToolStripMenuItem.Text = "New";
            this.newToolStripMenuItem.TextImageRelation = System.Windows.Forms.TextImageRelation.Overlay;
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(192, 6);
            // 
            // saveToolStripMenuItem
            // 
            this.saveToolStripMenuItem.Name = "saveToolStripMenuItem";
            this.saveToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S)));
            this.saveToolStripMenuItem.Size = new System.Drawing.Size(195, 22);
            this.saveToolStripMenuItem.Text = "Save";
            // 
            // saveAsToolStripMenuItem
            // 
            this.saveAsToolStripMenuItem.Name = "saveAsToolStripMenuItem";
            this.saveAsToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift) 
            | System.Windows.Forms.Keys.S)));
            this.saveAsToolStripMenuItem.Size = new System.Drawing.Size(195, 22);
            this.saveAsToolStripMenuItem.Text = "Save As...";
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(192, 6);
            // 
            // openToolStripMenuItem
            // 
            this.openToolStripMenuItem.Name = "openToolStripMenuItem";
            this.openToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.O)));
            this.openToolStripMenuItem.Size = new System.Drawing.Size(195, 22);
            this.openToolStripMenuItem.Text = "Open";
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(192, 6);
            // 
            // bioToolStripMenuItem
            // 
            this.bioToolStripMenuItem.Name = "bioToolStripMenuItem";
            this.bioToolStripMenuItem.Size = new System.Drawing.Size(195, 22);
            this.bioToolStripMenuItem.Text = "Bio";
            // 
            // CheckResult
            // 
            this.CheckResult.Controls.Add(this.CheckHitsResult);
            this.CheckResult.Controls.Add(this.CheckTime);
            this.CheckResult.Controls.Add(this.CheckProficiencyUsed);
            this.CheckResult.Controls.Add(this.D6);
            this.CheckResult.Controls.Add(this.D5);
            this.CheckResult.Controls.Add(this.D4);
            this.CheckResult.Controls.Add(this.D3);
            this.CheckResult.Controls.Add(this.D2);
            this.CheckResult.Controls.Add(this.D1);
            this.CheckResult.Location = new System.Drawing.Point(0, 257);
            this.CheckResult.Name = "CheckResult";
            this.CheckResult.Size = new System.Drawing.Size(353, 302);
            this.CheckResult.TabIndex = 1;
            this.CheckResult.TabStop = false;
            this.CheckResult.Text = "Check";
            this.CheckResult.Enter += new System.EventHandler(this.CheckResult_Enter);
            // 
            // CheckHitsResult
            // 
            this.CheckHitsResult.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.CheckHitsResult.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CheckHitsResult.Location = new System.Drawing.Point(-4, 244);
            this.CheckHitsResult.Name = "CheckHitsResult";
            this.CheckHitsResult.Size = new System.Drawing.Size(353, 25);
            this.CheckHitsResult.TabIndex = 8;
            this.CheckHitsResult.Text = "Rolled 5 Hits + 3 Adv = 8 Hits";
            this.CheckHitsResult.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // CheckTime
            // 
            this.CheckTime.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.CheckTime.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CheckTime.Location = new System.Drawing.Point(0, 277);
            this.CheckTime.Name = "CheckTime";
            this.CheckTime.Size = new System.Drawing.Size(353, 25);
            this.CheckTime.TabIndex = 7;
            this.CheckTime.Text = "Check made at: 00:00";
            this.CheckTime.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // CheckProficiencyUsed
            // 
            this.CheckProficiencyUsed.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.CheckProficiencyUsed.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CheckProficiencyUsed.Location = new System.Drawing.Point(0, 11);
            this.CheckProficiencyUsed.Name = "CheckProficiencyUsed";
            this.CheckProficiencyUsed.Size = new System.Drawing.Size(353, 21);
            this.CheckProficiencyUsed.TabIndex = 6;
            this.CheckProficiencyUsed.Text = "PROFICIENCY USED";
            this.CheckProficiencyUsed.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // D6
            // 
            this.D6.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.D6.Image = ((System.Drawing.Image)(resources.GetObject("D6.Image")));
            this.D6.InitialImage = ((System.Drawing.Image)(resources.GetObject("D6.InitialImage")));
            this.D6.Location = new System.Drawing.Point(230, 141);
            this.D6.MaximumSize = new System.Drawing.Size(100, 100);
            this.D6.MinimumSize = new System.Drawing.Size(100, 100);
            this.D6.Name = "D6";
            this.D6.Size = new System.Drawing.Size(100, 100);
            this.D6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.D6.TabIndex = 5;
            this.D6.TabStop = false;
            // 
            // D5
            // 
            this.D5.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.D5.Image = ((System.Drawing.Image)(resources.GetObject("D5.Image")));
            this.D5.InitialImage = ((System.Drawing.Image)(resources.GetObject("D5.InitialImage")));
            this.D5.Location = new System.Drawing.Point(124, 141);
            this.D5.MaximumSize = new System.Drawing.Size(100, 100);
            this.D5.MinimumSize = new System.Drawing.Size(100, 100);
            this.D5.Name = "D5";
            this.D5.Size = new System.Drawing.Size(100, 100);
            this.D5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.D5.TabIndex = 4;
            this.D5.TabStop = false;
            // 
            // D4
            // 
            this.D4.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.D4.Image = ((System.Drawing.Image)(resources.GetObject("D4.Image")));
            this.D4.InitialImage = ((System.Drawing.Image)(resources.GetObject("D4.InitialImage")));
            this.D4.Location = new System.Drawing.Point(18, 141);
            this.D4.MaximumSize = new System.Drawing.Size(100, 100);
            this.D4.MinimumSize = new System.Drawing.Size(100, 100);
            this.D4.Name = "D4";
            this.D4.Size = new System.Drawing.Size(100, 100);
            this.D4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.D4.TabIndex = 3;
            this.D4.TabStop = false;
            // 
            // D3
            // 
            this.D3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.D3.Image = ((System.Drawing.Image)(resources.GetObject("D3.Image")));
            this.D3.InitialImage = ((System.Drawing.Image)(resources.GetObject("D3.InitialImage")));
            this.D3.Location = new System.Drawing.Point(230, 35);
            this.D3.MaximumSize = new System.Drawing.Size(100, 100);
            this.D3.MinimumSize = new System.Drawing.Size(100, 100);
            this.D3.Name = "D3";
            this.D3.Size = new System.Drawing.Size(100, 100);
            this.D3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.D3.TabIndex = 2;
            this.D3.TabStop = false;
            // 
            // D2
            // 
            this.D2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.D2.Image = ((System.Drawing.Image)(resources.GetObject("D2.Image")));
            this.D2.InitialImage = ((System.Drawing.Image)(resources.GetObject("D2.InitialImage")));
            this.D2.Location = new System.Drawing.Point(124, 35);
            this.D2.MaximumSize = new System.Drawing.Size(100, 100);
            this.D2.MinimumSize = new System.Drawing.Size(100, 100);
            this.D2.Name = "D2";
            this.D2.Size = new System.Drawing.Size(100, 100);
            this.D2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.D2.TabIndex = 1;
            this.D2.TabStop = false;
            // 
            // D1
            // 
            this.D1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.D1.Image = ((System.Drawing.Image)(resources.GetObject("D1.Image")));
            this.D1.InitialImage = ((System.Drawing.Image)(resources.GetObject("D1.InitialImage")));
            this.D1.Location = new System.Drawing.Point(18, 35);
            this.D1.MaximumSize = new System.Drawing.Size(100, 100);
            this.D1.MinimumSize = new System.Drawing.Size(100, 100);
            this.D1.Name = "D1";
            this.D1.Size = new System.Drawing.Size(100, 100);
            this.D1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.D1.TabIndex = 0;
            this.D1.TabStop = false;
            // 
            // ProficiencyList
            // 
            this.ProficiencyList.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ProficiencyList.Location = new System.Drawing.Point(173, 55);
            this.ProficiencyList.Name = "ProficiencyList";
            this.ProficiencyList.Size = new System.Drawing.Size(135, 21);
            this.ProficiencyList.TabIndex = 5;
            this.ProficiencyList.SelectedIndexChanged += new System.EventHandler(this.ProficiencyList_SelectedIndexChanged);
            // 
            // AddProficiency
            // 
            this.AddProficiency.Location = new System.Drawing.Point(314, 55);
            this.AddProficiency.Name = "AddProficiency";
            this.AddProficiency.Size = new System.Drawing.Size(26, 21);
            this.AddProficiency.TabIndex = 3;
            this.AddProficiency.Text = "+";
            this.AddProficiency.UseVisualStyleBackColor = true;
            this.AddProficiency.Click += new System.EventHandler(this.AddProficiency_Click);
            // 
            // ProficiencyContainer
            // 
            this.ProficiencyContainer.Controls.Add(this.raceBonus);
            this.ProficiencyContainer.Controls.Add(this.ProfAdvLabel);
            this.ProficiencyContainer.Controls.Add(this.label9);
            this.ProficiencyContainer.Controls.Add(this.DecrProfAdv);
            this.ProficiencyContainer.Controls.Add(this.IncrProfAdv);
            this.ProficiencyContainer.Controls.Add(this.PointsAvailableLabel);
            this.ProficiencyContainer.Controls.Add(this.label3);
            this.ProficiencyContainer.Controls.Add(this.label2);
            this.ProficiencyContainer.Controls.Add(this.ProfDifficultyLabel);
            this.ProficiencyContainer.Controls.Add(this.ProfLevelLabel);
            this.ProficiencyContainer.Controls.Add(this.label1);
            this.ProficiencyContainer.Controls.Add(this.DecrProficiency);
            this.ProficiencyContainer.Controls.Add(this.AdvProficiency);
            this.ProficiencyContainer.Controls.Add(this.DeleteProficiency);
            this.ProficiencyContainer.Controls.Add(this.RollCheckButton);
            this.ProficiencyContainer.Location = new System.Drawing.Point(173, 82);
            this.ProficiencyContainer.Name = "ProficiencyContainer";
            this.ProficiencyContainer.Size = new System.Drawing.Size(180, 183);
            this.ProficiencyContainer.TabIndex = 4;
            this.ProficiencyContainer.TabStop = false;
            this.ProficiencyContainer.Text = "none";
            // 
            // raceBonus
            // 
            this.raceBonus.AutoSize = true;
            this.raceBonus.Location = new System.Drawing.Point(12, 35);
            this.raceBonus.Name = "raceBonus";
            this.raceBonus.Size = new System.Drawing.Size(85, 17);
            this.raceBonus.TabIndex = 14;
            this.raceBonus.Text = "Race Bonus";
            this.raceBonus.UseVisualStyleBackColor = true;
            this.raceBonus.CheckedChanged += new System.EventHandler(this.raceBonus_CheckedChanged);
            // 
            // ProfAdvLabel
            // 
            this.ProfAdvLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ProfAdvLabel.Location = new System.Drawing.Point(102, 80);
            this.ProfAdvLabel.Name = "ProfAdvLabel";
            this.ProfAdvLabel.Size = new System.Drawing.Size(39, 23);
            this.ProfAdvLabel.TabIndex = 13;
            this.ProfAdvLabel.Text = "5";
            this.ProfAdvLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(9, 84);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(62, 13);
            this.label9.TabIndex = 12;
            this.label9.Text = "Advantage:";
            // 
            // DecrProfAdv
            // 
            this.DecrProfAdv.Location = new System.Drawing.Point(71, 83);
            this.DecrProfAdv.Name = "DecrProfAdv";
            this.DecrProfAdv.Size = new System.Drawing.Size(25, 23);
            this.DecrProfAdv.TabIndex = 11;
            this.DecrProfAdv.Text = "-";
            this.DecrProfAdv.UseVisualStyleBackColor = true;
            this.DecrProfAdv.Click += new System.EventHandler(this.DecrProfAdv_Click);
            // 
            // IncrProfAdv
            // 
            this.IncrProfAdv.Location = new System.Drawing.Point(147, 83);
            this.IncrProfAdv.Name = "IncrProfAdv";
            this.IncrProfAdv.Size = new System.Drawing.Size(25, 23);
            this.IncrProfAdv.TabIndex = 10;
            this.IncrProfAdv.Text = "+";
            this.IncrProfAdv.UseVisualStyleBackColor = true;
            this.IncrProfAdv.Click += new System.EventHandler(this.IncrProfAdv_Click);
            // 
            // PointsAvailableLabel
            // 
            this.PointsAvailableLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PointsAvailableLabel.Location = new System.Drawing.Point(84, 11);
            this.PointsAvailableLabel.Name = "PointsAvailableLabel";
            this.PointsAvailableLabel.Size = new System.Drawing.Size(38, 23);
            this.PointsAvailableLabel.TabIndex = 9;
            this.PointsAvailableLabel.Text = "5";
            this.PointsAvailableLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(5, 18);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(84, 13);
            this.label3.TabIndex = 8;
            this.label3.Text = "Points available:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(9, 119);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(50, 13);
            this.label2.TabIndex = 7;
            this.label2.Text = "Difficulty:";
            // 
            // ProfDifficultyLabel
            // 
            this.ProfDifficultyLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ProfDifficultyLabel.Location = new System.Drawing.Point(88, 111);
            this.ProfDifficultyLabel.Name = "ProfDifficultyLabel";
            this.ProfDifficultyLabel.Size = new System.Drawing.Size(23, 23);
            this.ProfDifficultyLabel.TabIndex = 6;
            this.ProfDifficultyLabel.Text = "5";
            this.ProfDifficultyLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // ProfLevelLabel
            // 
            this.ProfLevelLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ProfLevelLabel.Location = new System.Drawing.Point(102, 51);
            this.ProfLevelLabel.Name = "ProfLevelLabel";
            this.ProfLevelLabel.Size = new System.Drawing.Size(39, 23);
            this.ProfLevelLabel.TabIndex = 5;
            this.ProfLevelLabel.Text = "5";
            this.ProfLevelLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 55);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(36, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Level:";
            // 
            // DecrProficiency
            // 
            this.DecrProficiency.Location = new System.Drawing.Point(71, 54);
            this.DecrProficiency.Name = "DecrProficiency";
            this.DecrProficiency.Size = new System.Drawing.Size(25, 23);
            this.DecrProficiency.TabIndex = 3;
            this.DecrProficiency.Text = "-";
            this.DecrProficiency.UseVisualStyleBackColor = true;
            this.DecrProficiency.Click += new System.EventHandler(this.DecrProficiency_Click);
            // 
            // AdvProficiency
            // 
            this.AdvProficiency.Location = new System.Drawing.Point(147, 54);
            this.AdvProficiency.Name = "AdvProficiency";
            this.AdvProficiency.Size = new System.Drawing.Size(25, 23);
            this.AdvProficiency.TabIndex = 2;
            this.AdvProficiency.Text = "+";
            this.AdvProficiency.UseVisualStyleBackColor = true;
            this.AdvProficiency.Click += new System.EventHandler(this.AdvProficiency_Click);
            // 
            // DeleteProficiency
            // 
            this.DeleteProficiency.Location = new System.Drawing.Point(128, 10);
            this.DeleteProficiency.Name = "DeleteProficiency";
            this.DeleteProficiency.Size = new System.Drawing.Size(46, 23);
            this.DeleteProficiency.TabIndex = 1;
            this.DeleteProficiency.Text = "Delete";
            this.DeleteProficiency.UseVisualStyleBackColor = true;
            this.DeleteProficiency.Click += new System.EventHandler(this.DeleteProficiency_Click);
            // 
            // RollCheckButton
            // 
            this.RollCheckButton.Location = new System.Drawing.Point(6, 142);
            this.RollCheckButton.Name = "RollCheckButton";
            this.RollCheckButton.Size = new System.Drawing.Size(168, 23);
            this.RollCheckButton.TabIndex = 0;
            this.RollCheckButton.Text = "Check";
            this.RollCheckButton.UseVisualStyleBackColor = true;
            this.RollCheckButton.Click += new System.EventHandler(this.RollCheckButton_Click);
            // 
            // OpenFileDialog
            // 
            this.OpenFileDialog.FileName = "character.66";
            // 
            // NameField
            // 
            this.NameField.Location = new System.Drawing.Point(13, 27);
            this.NameField.Name = "NameField";
            this.NameField.Size = new System.Drawing.Size(167, 20);
            this.NameField.TabIndex = 6;
            this.NameField.TextChanged += new System.EventHandler(this.NameField_TextChanged);
            // 
            // RaceField
            // 
            this.RaceField.Location = new System.Drawing.Point(186, 27);
            this.RaceField.Name = "RaceField";
            this.RaceField.Size = new System.Drawing.Size(154, 20);
            this.RaceField.TabIndex = 7;
            this.RaceField.TextChanged += new System.EventHandler(this.RaceField_TextChanged);
            // 
            // ExpLabel
            // 
            this.ExpLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ExpLabel.Location = new System.Drawing.Point(52, 177);
            this.ExpLabel.Name = "ExpLabel";
            this.ExpLabel.Size = new System.Drawing.Size(82, 23);
            this.ExpLabel.TabIndex = 10;
            this.ExpLabel.Text = "5";
            this.ExpLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // LowerExp
            // 
            this.LowerExp.Location = new System.Drawing.Point(18, 178);
            this.LowerExp.Name = "LowerExp";
            this.LowerExp.Size = new System.Drawing.Size(25, 23);
            this.LowerExp.TabIndex = 9;
            this.LowerExp.Text = "-";
            this.LowerExp.UseVisualStyleBackColor = true;
            this.LowerExp.Click += new System.EventHandler(this.LowerExp_Click);
            // 
            // IncrExp
            // 
            this.IncrExp.Location = new System.Drawing.Point(142, 178);
            this.IncrExp.Name = "IncrExp";
            this.IncrExp.Size = new System.Drawing.Size(25, 23);
            this.IncrExp.TabIndex = 8;
            this.IncrExp.Text = "+";
            this.IncrExp.UseVisualStyleBackColor = true;
            this.IncrExp.Click += new System.EventHandler(this.IncrExp_Click);
            // 
            // LevelLabel
            // 
            this.LevelLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LevelLabel.Location = new System.Drawing.Point(52, 217);
            this.LevelLabel.Name = "LevelLabel";
            this.LevelLabel.Size = new System.Drawing.Size(82, 23);
            this.LevelLabel.TabIndex = 13;
            this.LevelLabel.Text = "5";
            this.LevelLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // LowerLevel
            // 
            this.LowerLevel.Location = new System.Drawing.Point(18, 218);
            this.LowerLevel.Name = "LowerLevel";
            this.LowerLevel.Size = new System.Drawing.Size(25, 23);
            this.LowerLevel.TabIndex = 12;
            this.LowerLevel.Text = "-";
            this.LowerLevel.UseVisualStyleBackColor = true;
            this.LowerLevel.Click += new System.EventHandler(this.LowerLevel_Click);
            // 
            // IncrLevel
            // 
            this.IncrLevel.Location = new System.Drawing.Point(142, 218);
            this.IncrLevel.Name = "IncrLevel";
            this.IncrLevel.Size = new System.Drawing.Size(25, 23);
            this.IncrLevel.TabIndex = 11;
            this.IncrLevel.Text = "+";
            this.IncrLevel.UseVisualStyleBackColor = true;
            this.IncrLevel.Click += new System.EventHandler(this.IncrLevel_Click);
            // 
            // label5
            // 
            this.label5.Location = new System.Drawing.Point(49, 200);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(85, 21);
            this.label5.TabIndex = 6;
            this.label5.Text = "Level";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label6
            // 
            this.label6.Location = new System.Drawing.Point(49, 159);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(85, 21);
            this.label6.TabIndex = 14;
            this.label6.Text = "Experience";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // HealthBar
            // 
            this.HealthBar.Location = new System.Drawing.Point(12, 55);
            this.HealthBar.Name = "HealthBar";
            this.HealthBar.Size = new System.Drawing.Size(155, 21);
            this.HealthBar.TabIndex = 15;
            // 
            // label4
            // 
            this.label4.Location = new System.Drawing.Point(49, 119);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(85, 21);
            this.label4.TabIndex = 19;
            this.label4.Text = "Current Health";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // CurrentHealthLabel
            // 
            this.CurrentHealthLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CurrentHealthLabel.Location = new System.Drawing.Point(52, 137);
            this.CurrentHealthLabel.Name = "CurrentHealthLabel";
            this.CurrentHealthLabel.Size = new System.Drawing.Size(82, 23);
            this.CurrentHealthLabel.TabIndex = 18;
            this.CurrentHealthLabel.Text = "5";
            this.CurrentHealthLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // DecrHealthButton
            // 
            this.DecrHealthButton.Location = new System.Drawing.Point(18, 138);
            this.DecrHealthButton.Name = "DecrHealthButton";
            this.DecrHealthButton.Size = new System.Drawing.Size(25, 23);
            this.DecrHealthButton.TabIndex = 17;
            this.DecrHealthButton.Text = "-";
            this.DecrHealthButton.UseVisualStyleBackColor = true;
            this.DecrHealthButton.Click += new System.EventHandler(this.DecrHealthButton_Click);
            // 
            // IncrHealthButton
            // 
            this.IncrHealthButton.Location = new System.Drawing.Point(142, 138);
            this.IncrHealthButton.Name = "IncrHealthButton";
            this.IncrHealthButton.Size = new System.Drawing.Size(25, 23);
            this.IncrHealthButton.TabIndex = 16;
            this.IncrHealthButton.Text = "+";
            this.IncrHealthButton.UseVisualStyleBackColor = true;
            this.IncrHealthButton.Click += new System.EventHandler(this.IncrHealthButton_Click);
            // 
            // label8
            // 
            this.label8.Location = new System.Drawing.Point(49, 79);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(85, 21);
            this.label8.TabIndex = 23;
            this.label8.Text = "Maximum Health";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // MaximumHealthLabel
            // 
            this.MaximumHealthLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MaximumHealthLabel.Location = new System.Drawing.Point(52, 97);
            this.MaximumHealthLabel.Name = "MaximumHealthLabel";
            this.MaximumHealthLabel.Size = new System.Drawing.Size(82, 23);
            this.MaximumHealthLabel.TabIndex = 22;
            this.MaximumHealthLabel.Text = "5";
            this.MaximumHealthLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // DecrMaxHealthButton
            // 
            this.DecrMaxHealthButton.Location = new System.Drawing.Point(18, 98);
            this.DecrMaxHealthButton.Name = "DecrMaxHealthButton";
            this.DecrMaxHealthButton.Size = new System.Drawing.Size(25, 23);
            this.DecrMaxHealthButton.TabIndex = 21;
            this.DecrMaxHealthButton.Text = "-";
            this.DecrMaxHealthButton.UseVisualStyleBackColor = true;
            this.DecrMaxHealthButton.Click += new System.EventHandler(this.DecrMaxHealthButton_Click);
            // 
            // IncrMaxHealthButton
            // 
            this.IncrMaxHealthButton.Location = new System.Drawing.Point(142, 98);
            this.IncrMaxHealthButton.Name = "IncrMaxHealthButton";
            this.IncrMaxHealthButton.Size = new System.Drawing.Size(25, 23);
            this.IncrMaxHealthButton.TabIndex = 20;
            this.IncrMaxHealthButton.Text = "+";
            this.IncrMaxHealthButton.UseVisualStyleBackColor = true;
            this.IncrMaxHealthButton.Click += new System.EventHandler(this.IncrMaxHealthButton_Click);
            // 
            // InventoryControl
            // 
            this.InventoryControl.Controls.Add(this.tabPage2);
            this.InventoryControl.Controls.Add(this.tabPage1);
            this.InventoryControl.Location = new System.Drawing.Point(356, 27);
            this.InventoryControl.Name = "InventoryControl";
            this.InventoryControl.SelectedIndex = 0;
            this.InventoryControl.Size = new System.Drawing.Size(177, 532);
            this.InventoryControl.TabIndex = 24;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.SelectedItemName);
            this.tabPage2.Controls.Add(this.AddItem);
            this.tabPage2.Controls.Add(this.RemoveItem);
            this.tabPage2.Controls.Add(this.InventoryItemEquipped);
            this.tabPage2.Controls.Add(this.InventoryList);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Size = new System.Drawing.Size(169, 506);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Inventory";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // SelectedItemName
            // 
            this.SelectedItemName.Location = new System.Drawing.Point(7, 432);
            this.SelectedItemName.Name = "SelectedItemName";
            this.SelectedItemName.Size = new System.Drawing.Size(154, 20);
            this.SelectedItemName.TabIndex = 4;
            this.SelectedItemName.TextChanged += new System.EventHandler(this.SelectedItemName_TextChanged);
            // 
            // AddItem
            // 
            this.AddItem.Location = new System.Drawing.Point(87, 451);
            this.AddItem.Name = "AddItem";
            this.AddItem.Size = new System.Drawing.Size(75, 23);
            this.AddItem.TabIndex = 3;
            this.AddItem.Text = "New Item";
            this.AddItem.UseVisualStyleBackColor = true;
            this.AddItem.Click += new System.EventHandler(this.AddItem_Click);
            // 
            // RemoveItem
            // 
            this.RemoveItem.Location = new System.Drawing.Point(6, 451);
            this.RemoveItem.Name = "RemoveItem";
            this.RemoveItem.Size = new System.Drawing.Size(75, 23);
            this.RemoveItem.TabIndex = 2;
            this.RemoveItem.Text = "Delete Item";
            this.RemoveItem.UseVisualStyleBackColor = true;
            this.RemoveItem.Click += new System.EventHandler(this.RemoveItem_Click);
            // 
            // InventoryItemEquipped
            // 
            this.InventoryItemEquipped.AutoSize = true;
            this.InventoryItemEquipped.Location = new System.Drawing.Point(3, 482);
            this.InventoryItemEquipped.Name = "InventoryItemEquipped";
            this.InventoryItemEquipped.Size = new System.Drawing.Size(71, 17);
            this.InventoryItemEquipped.TabIndex = 1;
            this.InventoryItemEquipped.Text = "Equipped";
            this.InventoryItemEquipped.UseVisualStyleBackColor = true;
            this.InventoryItemEquipped.CheckedChanged += new System.EventHandler(this.InventoryItemEquipped_CheckedChanged);
            // 
            // InventoryList
            // 
            this.InventoryList.Dock = System.Windows.Forms.DockStyle.Top;
            listViewGroup1.Header = "Equipped";
            listViewGroup1.Name = "EquippedItemList";
            listViewGroup2.Header = "Carried";
            listViewGroup2.Name = "CarriedItemList";
            this.InventoryList.Groups.AddRange(new System.Windows.Forms.ListViewGroup[] {
            listViewGroup1,
            listViewGroup2});
            listViewItem1.Group = listViewGroup1;
            listViewItem2.Group = listViewGroup2;
            this.InventoryList.Items.AddRange(new System.Windows.Forms.ListViewItem[] {
            listViewItem1,
            listViewItem2});
            this.InventoryList.Location = new System.Drawing.Point(0, 0);
            this.InventoryList.Margin = new System.Windows.Forms.Padding(0);
            this.InventoryList.Name = "InventoryList";
            this.InventoryList.Size = new System.Drawing.Size(169, 427);
            this.InventoryList.TabIndex = 0;
            this.InventoryList.UseCompatibleStateImageBehavior = false;
            this.InventoryList.View = System.Windows.Forms.View.SmallIcon;
            this.InventoryList.SelectedIndexChanged += new System.EventHandler(this.InventoryList_SelectedIndexChanged);
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.SpellName);
            this.tabPage1.Controls.Add(this.NewSpell);
            this.tabPage1.Controls.Add(this.DeleteSpell);
            this.tabPage1.Controls.Add(this.SpellLevel);
            this.tabPage1.Controls.Add(this.label10);
            this.tabPage1.Controls.Add(this.SpellLevelDecrease);
            this.tabPage1.Controls.Add(this.SpellLevelIncrease);
            this.tabPage1.Controls.Add(this.ArcaneList);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Size = new System.Drawing.Size(169, 506);
            this.tabPage1.TabIndex = 2;
            this.tabPage1.Text = "Spells";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // SpellName
            // 
            this.SpellName.Location = new System.Drawing.Point(7, 432);
            this.SpellName.Name = "SpellName";
            this.SpellName.Size = new System.Drawing.Size(154, 20);
            this.SpellName.TabIndex = 12;
            this.SpellName.TextChanged += new System.EventHandler(this.SpellName_TextChanged);
            // 
            // NewSpell
            // 
            this.NewSpell.Location = new System.Drawing.Point(87, 451);
            this.NewSpell.Name = "NewSpell";
            this.NewSpell.Size = new System.Drawing.Size(75, 23);
            this.NewSpell.TabIndex = 11;
            this.NewSpell.Text = "New Spell";
            this.NewSpell.UseVisualStyleBackColor = true;
            this.NewSpell.Click += new System.EventHandler(this.NewSpell_Click);
            // 
            // DeleteSpell
            // 
            this.DeleteSpell.Location = new System.Drawing.Point(6, 451);
            this.DeleteSpell.Name = "DeleteSpell";
            this.DeleteSpell.Size = new System.Drawing.Size(75, 23);
            this.DeleteSpell.TabIndex = 10;
            this.DeleteSpell.Text = "Delete Spell";
            this.DeleteSpell.UseVisualStyleBackColor = true;
            this.DeleteSpell.Click += new System.EventHandler(this.DeleteSpell_Click);
            // 
            // SpellLevel
            // 
            this.SpellLevel.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SpellLevel.Location = new System.Drawing.Point(96, 477);
            this.SpellLevel.Name = "SpellLevel";
            this.SpellLevel.Size = new System.Drawing.Size(39, 23);
            this.SpellLevel.TabIndex = 9;
            this.SpellLevel.Text = "-";
            this.SpellLevel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(12, 485);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(36, 13);
            this.label10.TabIndex = 8;
            this.label10.Text = "Level:";
            // 
            // SpellLevelDecrease
            // 
            this.SpellLevelDecrease.Location = new System.Drawing.Point(65, 480);
            this.SpellLevelDecrease.Name = "SpellLevelDecrease";
            this.SpellLevelDecrease.Size = new System.Drawing.Size(25, 23);
            this.SpellLevelDecrease.TabIndex = 7;
            this.SpellLevelDecrease.Text = "-";
            this.SpellLevelDecrease.UseVisualStyleBackColor = true;
            this.SpellLevelDecrease.Click += new System.EventHandler(this.SpellLevelDecrease_Click);
            // 
            // SpellLevelIncrease
            // 
            this.SpellLevelIncrease.Location = new System.Drawing.Point(141, 480);
            this.SpellLevelIncrease.Name = "SpellLevelIncrease";
            this.SpellLevelIncrease.Size = new System.Drawing.Size(25, 23);
            this.SpellLevelIncrease.TabIndex = 6;
            this.SpellLevelIncrease.Text = "+";
            this.SpellLevelIncrease.UseVisualStyleBackColor = true;
            this.SpellLevelIncrease.Click += new System.EventHandler(this.SpellLevelIncrease_Click);
            // 
            // ArcaneList
            // 
            this.ArcaneList.Dock = System.Windows.Forms.DockStyle.Top;
            listViewGroup3.Header = "Level 1";
            listViewGroup3.Name = "EquippedItemList";
            listViewGroup4.Header = "Level 2";
            listViewGroup4.Name = "CarriedItemList";
            listViewGroup5.Header = "Level 3";
            listViewGroup5.Name = "listViewGroup2";
            listViewGroup6.Header = "Level 4";
            listViewGroup6.Name = "listViewGroup3";
            listViewGroup7.Header = "Level 5";
            listViewGroup7.Name = "listViewGroup4";
            listViewGroup8.Header = "Level 6";
            listViewGroup8.Name = "listViewGroup5";
            this.ArcaneList.Groups.AddRange(new System.Windows.Forms.ListViewGroup[] {
            listViewGroup3,
            listViewGroup4,
            listViewGroup5,
            listViewGroup6,
            listViewGroup7,
            listViewGroup8});
            listViewItem3.Group = listViewGroup3;
            listViewItem4.Group = listViewGroup4;
            this.ArcaneList.Items.AddRange(new System.Windows.Forms.ListViewItem[] {
            listViewItem3,
            listViewItem4});
            this.ArcaneList.Location = new System.Drawing.Point(0, 0);
            this.ArcaneList.Name = "ArcaneList";
            this.ArcaneList.Size = new System.Drawing.Size(169, 427);
            this.ArcaneList.TabIndex = 1;
            this.ArcaneList.UseCompatibleStateImageBehavior = false;
            this.ArcaneList.View = System.Windows.Forms.View.SmallIcon;
            this.ArcaneList.SelectedIndexChanged += new System.EventHandler(this.ArcaneList_SelectedIndexChanged);
            // 
            // ExperienceBar
            // 
            this.ExperienceBar.Location = new System.Drawing.Point(18, 243);
            this.ExperienceBar.Name = "ExperienceBar";
            this.ExperienceBar.Size = new System.Drawing.Size(149, 10);
            this.ExperienceBar.TabIndex = 25;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(533, 559);
            this.Controls.Add(this.ExperienceBar);
            this.Controls.Add(this.InventoryControl);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.MaximumHealthLabel);
            this.Controls.Add(this.DecrMaxHealthButton);
            this.Controls.Add(this.IncrMaxHealthButton);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.CurrentHealthLabel);
            this.Controls.Add(this.DecrHealthButton);
            this.Controls.Add(this.IncrHealthButton);
            this.Controls.Add(this.HealthBar);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.LevelLabel);
            this.Controls.Add(this.LowerLevel);
            this.Controls.Add(this.IncrLevel);
            this.Controls.Add(this.ExpLabel);
            this.Controls.Add(this.LowerExp);
            this.Controls.Add(this.IncrExp);
            this.Controls.Add(this.RaceField);
            this.Controls.Add(this.NameField);
            this.Controls.Add(this.ProficiencyContainer);
            this.Controls.Add(this.AddProficiency);
            this.Controls.Add(this.ProficiencyList);
            this.Controls.Add(this.CheckResult);
            this.Controls.Add(this.menuStrip1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.Text = "66 Character Manager";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.CheckResult.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.D6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.D5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.D4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.D3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.D2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.D1)).EndInit();
            this.ProficiencyContainer.ResumeLayout(false);
            this.ProficiencyContainer.PerformLayout();
            this.InventoryControl.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.proficiencyBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.moveBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.proficiencyBindingSource1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.proficiencyBindingSource2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem characterToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem newToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripMenuItem saveToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveAsToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem openToolStripMenuItem;
        private System.Windows.Forms.GroupBox CheckResult;
        private System.Windows.Forms.BindingSource proficiencyBindingSource;
        private System.Windows.Forms.BindingSource moveBindingSource;
        private System.Windows.Forms.ComboBox ProficiencyList;
        private System.Windows.Forms.BindingSource proficiencyBindingSource1;
        private System.Windows.Forms.BindingSource proficiencyBindingSource2;
        private System.Windows.Forms.Button AddProficiency;
        private System.Windows.Forms.GroupBox ProficiencyContainer;
        private System.Windows.Forms.Button DeleteProficiency;
        private System.Windows.Forms.Button RollCheckButton;
        private System.Windows.Forms.SaveFileDialog SaveFileDialog;
        private System.Windows.Forms.OpenFileDialog OpenFileDialog;
        private System.Windows.Forms.PictureBox D1;
        private System.Windows.Forms.PictureBox D6;
        private System.Windows.Forms.PictureBox D5;
        private System.Windows.Forms.PictureBox D4;
        private System.Windows.Forms.PictureBox D3;
        private System.Windows.Forms.PictureBox D2;
        private System.Windows.Forms.Label CheckTime;
        private System.Windows.Forms.Label CheckProficiencyUsed;
        private System.Windows.Forms.Label ProfLevelLabel;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button DecrProficiency;
        private System.Windows.Forms.Button AdvProficiency;
        private System.Windows.Forms.TextBox NameField;
        private System.Windows.Forms.TextBox RaceField;
        private System.Windows.Forms.Label ExpLabel;
        private System.Windows.Forms.Button LowerExp;
        private System.Windows.Forms.Button IncrExp;
        private System.Windows.Forms.Label LevelLabel;
        private System.Windows.Forms.Button LowerLevel;
        private System.Windows.Forms.Button IncrLevel;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label ProfDifficultyLabel;
        private System.Windows.Forms.Label PointsAvailableLabel;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label CheckHitsResult;
        private System.Windows.Forms.ProgressBar HealthBar;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label CurrentHealthLabel;
        private System.Windows.Forms.Button DecrHealthButton;
        private System.Windows.Forms.Button IncrHealthButton;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label MaximumHealthLabel;
        private System.Windows.Forms.Button DecrMaxHealthButton;
        private System.Windows.Forms.Button IncrMaxHealthButton;
        private System.Windows.Forms.Label ProfAdvLabel;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Button DecrProfAdv;
        private System.Windows.Forms.Button IncrProfAdv;
        private System.Windows.Forms.TabControl InventoryControl;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TextBox SelectedItemName;
        private System.Windows.Forms.Button AddItem;
        private System.Windows.Forms.Button RemoveItem;
        private System.Windows.Forms.CheckBox InventoryItemEquipped;
        private System.Windows.Forms.ListView InventoryList;
        private System.Windows.Forms.ListView ArcaneList;
        private System.Windows.Forms.TextBox SpellName;
        private System.Windows.Forms.Button NewSpell;
        private System.Windows.Forms.Button DeleteSpell;
        private System.Windows.Forms.Label SpellLevel;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Button SpellLevelDecrease;
        private System.Windows.Forms.Button SpellLevelIncrease;
        private System.Windows.Forms.ProgressBar ExperienceBar;
        private System.Windows.Forms.CheckBox raceBonus;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripMenuItem bioToolStripMenuItem;
    }
}

