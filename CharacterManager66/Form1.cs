﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CharacterManager66
{
    public partial class Form1 : Form
    {
        private bool dead = false;
        private int rerolledCount = 1;
        private string prevText = "";
        private string prevProf = "";

        private Random random = new Random();
        public static Form1 Instance;
        public static Character Character = new Character();

        private ListViewItem selectedInventory;
        private ListViewItem selectedArcane;

        private bool refreshProfDropDownOverride = true;

        public Form1()
        {
            Instance = this;
            InitializeComponent();
            Character.LoadFromArgs();

            RefreshAll();
        }

        private void RefreshAll()
        {
            RefreshUI();
            RefreshInventory();
            RefreshSpells();
        }

        private void RefreshUI()
        {
            ProficiencyList.DataSource = null;
            ProficiencyList.DataSource = Character.Data.Proficiencies;
            
            Text = Character.Data.Name + " - Lv" + Character.Data.Level;

            NameField.Text = Character.Data.Name;
            RaceField.Text = Character.Data.Race;

            ExpLabel.Text = Character.Data.Experience.ToString();
            LevelLabel.Text = Character.Data.Level.ToString();

            if(ProficiencyList.SelectedIndex >= 0)
                RefreshProficiencies();

            PointsAvailableLabel.Text = Character.Data.PointsRemaining.ToString();
            
            CheckTime.Visible = false;
            CheckProficiencyUsed.Visible = false;
            CheckHitsResult.Visible = false;

            CurrentHealthLabel.Text = Character.Data.Health.ToString();
            MaximumHealthLabel.Text = Character.Data.MaxHealth.ToString();

            if (Character.Data.Health >= 0)
            {
                HealthBar.Maximum = Character.Data.MaxHealth * 100;
                HealthBar.Value = (Character.Data.Health > Character.Data.MaxHealth? Character.Data.MaxHealth : Character.Data.Health) * 100;
            }

            if (Character.Data.Experience >= 0)
            {
                ExperienceBar.Maximum = (Character.Data.Level + 1) * 100;
                ExperienceBar.Value = (Character.Data.Experience > (Character.Data.Level + 1) ? Character.Data.Level + 1 : Character.Data.Experience) * 100;
            }
        }

        private void RefreshProficiencies()
        {
            CharacterData.Proficiency proficiency = Character.Data.Proficiencies[ProficiencyList.SelectedIndex];

            int level = proficiency.Level;

            D2.Visible = level > 1 - (proficiency.RaceBonus? 1 : 0);
            D3.Visible = level > 2 - (proficiency.RaceBonus ? 1 : 0);
            D4.Visible = level > 3 - (proficiency.RaceBonus ? 1 : 0);
            D5.Visible = level > 4 - (proficiency.RaceBonus ? 1 : 0);
            D6.Visible = level > 5 - (proficiency.RaceBonus ? 1 : 0);

            ProfLevelLabel.Text = (proficiency.Level + (proficiency.RaceBonus? 1 : 0)).ToString();
            ProfDifficultyLabel.Text = proficiency.Difficulty.ToString();

            AdvProficiency.Enabled = proficiency.Level + (proficiency.RaceBonus ? 1 : 0) < 6 && Character.Data.PointsRemaining > 0 && ProficiencyList.SelectedIndex > 0;
            DecrProficiency.Enabled = proficiency.Level > 0 && ProficiencyList.SelectedIndex > 0;
            
            ProfAdvLabel.Text = proficiency.Advantage.ToString();

            refreshProfDropDownOverride = false;

            raceBonus.Checked = proficiency.RaceBonus;

            if(!proficiency.RaceBonus)
            {
                raceBonus.Visible = !proficiency.Name.Equals("Unskilled");

                foreach (CharacterData.Proficiency prof in Character.Data.Proficiencies)
                {
                    if (prof != proficiency && prof.RaceBonus)
                        raceBonus.Visible = false;
                }
            }
            else
            {
                raceBonus.Visible = true;
            }

            refreshProfDropDownOverride = true;
        }

        private void characterToolStripMenuItem_DropDownItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            Console.WriteLine(e.ClickedItem.Text);

            characterToolStripMenuItem.HideDropDown();
            
            switch (e.ClickedItem.Text)
            {
                case "New":
                    Character = new Character();
                    RefreshAll();
                    break;
                case "Save":
                    if (Character.HasFilePath())
                    {
                        Character.Save();
                    }
                    else
                    {
                        SaveAs();
                    }
                    break;
                case "Save As...":
                    SaveAs();
                    break;
                case "Open":
                    Open();
                    break;
                case "Bio":
                    OpenBio();
                    break;
            }
            
            RefreshUI();
        }

        private void OpenBio()
        {
            BioForm bioForm = new BioForm();

            bioForm.ShowDialog();
        }

        private void AddProficiency_Click(object sender, EventArgs e)
        {
            TextInputDialog dialog = new TextInputDialog();

            dialog.SetTitle("New Proficiency");
            dialog.SetText("Enter the name of the new Proficiency");

            dialog.ShowDialog();

            RefreshUI();
        }

        public void AddNewProficiency(string name)
        {
            CharacterData.Proficiency existing = Character.Data.Proficiencies.Find(o => o.Name.Equals(name));

            if (existing == null)
            {
                CharacterData.Proficiency newProf = new CharacterData.Proficiency();
                newProf.Name = name;

                Character.Data.Proficiencies.Add(newProf);

                RefreshUI();

                ProficiencyList.SelectedIndex = Character.Data.Proficiencies.Count - 1;
            }
            else
            {
                ProficiencyList.SelectedIndex = Character.Data.Proficiencies.IndexOf(existing);
            }
        }

        private void DeleteProficiency_Click(object sender, EventArgs e)
        {
            DialogResult confirmResult = MessageBox.Show("Are you sure to delete this proficiency?", "Confirm Delete", MessageBoxButtons.YesNo);

            if (confirmResult == DialogResult.Yes)
            {
                Character.Data.Proficiencies.RemoveAt(ProficiencyList.SelectedIndex);
                ProficiencyList.SelectedIndex = 0;
            }
            
            RefreshUI();
        }

        private void ProficiencyList_SelectedIndexChanged(object sender, EventArgs e)
        {
            DeleteProficiency.Enabled = ProficiencyList.SelectedIndex != 0;
            DeleteProficiency.Visible = ProficiencyList.SelectedIndex != 0;

            if (Character.Data.Proficiencies.Count > ProficiencyList.SelectedIndex && ProficiencyList.SelectedIndex >= 0)
                ProficiencyContainer.Text = Character.Data.Proficiencies[ProficiencyList.SelectedIndex].Name;

            if (ProficiencyList.SelectedIndex >= 0)
                RefreshProficiencies();
        }

        private void Open()
        {
            OpenFileDialog.Filter = "66 Character File|*.66";

            DialogResult result = OpenFileDialog.ShowDialog();

            if (result != DialogResult.OK)
                return;

            Character.LoadFromFile(OpenFileDialog.FileName);

            RefreshAll();
        }

        private void SaveAs()
        {
            SaveFileDialog.FileName = Character.FileName;

            SaveFileDialog.Filter = "66 Character File|*.66";

            DialogResult result = SaveFileDialog.ShowDialog();

            if (result != DialogResult.OK)
                return;

            Character.SaveAs(SaveFileDialog.FileName);
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            DialogResult confirmResult = MessageBox.Show("Do you want to save your character before exiting?", "Save?", dead? MessageBoxButtons.YesNo : MessageBoxButtons.YesNoCancel);

            if (confirmResult == DialogResult.Yes)
            {
                SaveAs();
            }
            
            e.Cancel = confirmResult == DialogResult.Cancel;
        }

        private void RollCheckButton_Click(object sender, EventArgs e)
        {
            Roll(false, 0, 0, 0, 0, 0, 0);

            RollCheckButton.Enabled = false;
            ProficiencyList.Enabled = false;
            DeleteProficiency.Enabled = false;
            AddProficiency.Enabled = false;
            IncrProfAdv.Enabled = false;
            DecrProfAdv.Enabled = false;
        }

        private void Roll(bool reroll, int od1, int od2, int od3, int od4, int od5, int od6)
        { 
            CharacterData.Proficiency proficiency = Character.Data.Proficiencies[ProficiencyList.SelectedIndex];

            int level = proficiency.Level + (proficiency.RaceBonus ? 1 : 0);

            D2.Visible = level > 1;
            D3.Visible = level > 2;
            D4.Visible = level > 3;
            D5.Visible = level > 4;
            D6.Visible = level > 5;

            Thread roller = new Thread(() =>
            { 
                
                int d1 = reroll ? od1 : random.Next(1, 7);
                int d2 = reroll ? od2 : random.Next(1, 7);
                int d3 = reroll ? od3 : random.Next(1, 7);
                int d4 = reroll ? od4 : random.Next(1, 7);
                int d5 = reroll ? od5 : random.Next(1, 7);
                int d6 = reroll ? od6 : random.Next(1, 7);

                if (reroll)
                {
                    bool done = false;

                    if (d1 == 6 && !done)
                    {
                        d1 = 7;
                        done = true;
                    }

                    if (d2 == 6 && !done)
                    {
                        d2 = 7;
                        done = true;
                    }

                    if (d3 == 6 && !done)
                    {
                        d3 = 7;
                        done = true;
                    }

                    if (d4 == 6 && !done)
                    {
                        d4 = 7;
                        done = true;
                    }

                    if (d5 == 6 && !done)
                    {
                        d5 = 7;
                        done = true;
                    }

                    if (d6 == 6 && !done)
                    {
                        d6 = 7;
                        done = true;
                    }
                }

                int maxVal = 20;
                int minVal = 10;
                
                bool found = false;
                bool reroll1 = false;
                bool reroll2 = false;
                bool reroll3 = false;
                bool reroll4 = false;
                bool reroll5 = false;
                bool reroll6 = false;

                if(reroll)
                {
                    if(od1 < 4 && !found)
                    {
                        found = true;
                        reroll1 = true;
                        d1 = random.Next(1, 7);
                    }

                    if (od2 < 4 && !found)
                    {
                        found = true;
                        reroll2 = true;
                        d2 = random.Next(1, 7);
                    }

                    if (od3 < 4 && !found)
                    {
                        found = true;
                        reroll3 = true;
                        d3 = random.Next(1, 7);
                    }

                    if (od4 < 4 && !found)
                    {
                        found = true;
                        reroll4 = true;
                        d4 = random.Next(1, 7);
                    }

                    if (od5 < 4 && !found)
                    {
                        found = true;
                        reroll5 = true;
                        d5 = random.Next(1, 7);
                    }

                    if (od6 < 4 && !found)
                    {
                        found = true;
                        reroll6 = true;
                        d6 = random.Next(1, 7);
                    }
                }

                int d1r = !reroll1 && reroll ? od1 : minVal + random.Next(maxVal);
                int d2r = !reroll2 && reroll ? od2 : minVal + random.Next(maxVal);
                int d3r = !reroll3 && reroll ? od3 : minVal + random.Next(maxVal);
                int d4r = !reroll4 && reroll ? od4 : minVal + random.Next(maxVal);
                int d5r = !reroll5 && reroll ? od5 : minVal + random.Next(maxVal);
                int d6r = !reroll6 && reroll ? od6 : minVal + random.Next(maxVal);

                double p = 1;

                Stopwatch timer = new Stopwatch();

                double dT = 0;

                while(p > 0)
                {
                    timer.Start();

                    p -= dT * 0.01;
                    
                    if (!reroll || reroll1)
                        SetDice(D1, (Lerp(d1, d1r, p*p) % 6) + 1, level == 0);

                    if (!reroll || reroll2)
                        SetDice(D2, (Lerp(d2, d2r, p*p) % 6) + 1, level == 0);

                    if (!reroll || reroll3)
                        SetDice(D3, (Lerp(d3, d3r, p*p) % 6) + 1, level == 0);

                    if (!reroll || reroll4)
                        SetDice(D4, (Lerp(d4, d4r, p*p) % 6) + 1, level == 0);

                    if (!reroll || reroll5)
                        SetDice(D5, (Lerp(d5, d5r, p*p) % 6) + 1, level == 0);

                    if (!reroll || reroll6)
                        SetDice(D6, (Lerp(d6, d6r, p*p) % 6) + 1, level == 0);

                    timer.Stop();
                    dT = (double)timer.ElapsedMilliseconds * 0.001;
                }

                SetDice(D1, d1, level == 0 || proficiency.Name.Equals("Unskilled"));
                SetDice(D2, d2, level == 0 || proficiency.Name.Equals("Unskilled"));
                SetDice(D3, d3, level == 0 || proficiency.Name.Equals("Unskilled"));
                SetDice(D4, d4, level == 0 || proficiency.Name.Equals("Unskilled"));
                SetDice(D5, d5, level == 0 || proficiency.Name.Equals("Unskilled"));
                SetDice(D6, d6, level == 0 || proficiency.Name.Equals("Unskilled"));

                int sixes = 0;
                int fails = 0;

                if (d1 == 6)
                    sixes++;

                if (d2 == 6 && level > 1)
                    sixes++;

                if (d3 == 6 && level > 2)
                    sixes++;

                if (d4 == 6 && level > 3)
                    sixes++;

                if (d5 == 6 && level > 4)
                    sixes++;

                if (d6 == 6 && level > 5)
                    sixes++;

                if (d1 <= 3)
                    fails++;

                if (d2 <= 3 && level > 1)
                    fails++;

                if (d3 <= 3 && level > 2)
                    fails++;

                if (d4 <= 3 && level > 3)
                    fails++;

                if (d5 <= 3 && level > 4)
                    fails++;

                if (d6 <= 3 && level > 5)
                    fails++;

                if(sixes > 0 && fails > 0)
                {
                    Thread.Sleep(1000);

                    NameField.Invoke((MethodInvoker)delegate
                    {
                        Roll(true, d1, d2, d3, d4, d5, d6);
                    });
                }
                else
                {
                    NameField.Invoke((MethodInvoker)delegate
                    {
                        string text = DateTime.Now.ToString("HH:mm");

                        if(prevText.Equals(text) && proficiency.Name.Equals(prevProf))
                        {
                            rerolledCount++;
                        }
                        else
                        {
                            rerolledCount = 1;
                        }

                        prevText = text;
                        prevProf = proficiency.Name;

                        CheckTime.Visible = true;
                        CheckProficiencyUsed.Visible = true;
                        CheckHitsResult.Visible = true;

                        CheckProficiencyUsed.Text = proficiency.Name;
                        CheckTime.Text = text + (rerolledCount > 1? " (Attempt " + rerolledCount + ")":"");
                        
                        RollCheckButton.Enabled = true;
                        ProficiencyList.Enabled = true;
                        DeleteProficiency.Enabled = true;
                        AddProficiency.Enabled = true;
                        IncrProfAdv.Enabled = true;
                        DecrProfAdv.Enabled = true;

                        int hits = 0;

                        if (d1 > 3)
                            hits++;

                        if (d2 > 3 && level > 1)
                            hits++;

                        if (d3 > 3 && level > 2)
                            hits++;

                        if (d4 > 3 && level > 3)
                            hits++;

                        if (d5 > 3 && level > 4)
                            hits++;

                        if (d6 > 3 && level > 5)
                            hits++;

                        if (d1 == 6) SetDice(D1, 7, false);
                        if (d2 == 6) SetDice(D2, 7, false);
                        if (d3 == 6) SetDice(D3, 7, false);
                        if (d4 == 6) SetDice(D4, 7, false);
                        if (d5 == 6) SetDice(D5, 7, false);
                        if (d6 == 6) SetDice(D6, 7, false);

                        int result = hits + Character.Data.Proficiencies[ProficiencyList.SelectedIndex].Advantage;

                        CheckHitsResult.Text = "Rolled " + hits + " Hits + " + Character.Data.Proficiencies[ProficiencyList.SelectedIndex].Advantage + " Adv = " + result + " Hits.";

                        Bitmap bmp = new Bitmap(CheckResult.Width, CheckResult.Height);

                        CheckResult.DrawToBitmap(bmp, new Rectangle(Point.Empty, CheckResult.Size));

                        Clipboard.SetImage(bmp);

                        CheckTime.Text += " (copied to clipboard)";
                    });
                }
            });

            roller.Start();
        }

        private int Lerp(int start, int to, double t)
        {
            return (int)Math.Round((double)start + (((double)to - (double)start) * t));
        }

        private void SetDice(PictureBox D, int d, bool fourIsBad)
        {
            if(d == 1)
                D.Image = CharacterManager66.Properties.Resources.dice_1;

            if (d == 2)
                D.Image = CharacterManager66.Properties.Resources.dice_2;

            if (d == 3)
                D.Image = CharacterManager66.Properties.Resources.dice_3;

            if (d == 4)
            {
                D.Image = CharacterManager66.Properties.Resources.dice_4_g;

                if (fourIsBad)
                    D.Image = CharacterManager66.Properties.Resources.dice_4_b;
            }

            if (d == 5)
                D.Image = CharacterManager66.Properties.Resources.dice_5;

            if (d == 6)
                D.Image = CharacterManager66.Properties.Resources.dice_6;

            if (d == 7) //re-rolled 6
                D.Image = CharacterManager66.Properties.Resources.dice_6_r;
        }

        private void CheckResult_Enter(object sender, EventArgs e)
        {

        }

        private void NameField_TextChanged(object sender, EventArgs e)
        {
            Character.Data.Name = NameField.Text;
            RefreshUI();
        }

        private void RaceField_TextChanged(object sender, EventArgs e)
        {
            Character.Data.Race = RaceField.Text;
            RefreshUI();
        }

        private void IncrExp_Click(object sender, EventArgs e)
        {
            Character.Data.Experience++;
            RefreshUI();
        }

        private void LowerExp_Click(object sender, EventArgs e)
        {
            Character.Data.Experience--;
            RefreshUI();
        }

        private void IncrLevel_Click(object sender, EventArgs e)
        {
            Character.Data.AddLevel();
            RefreshUI();
        }

        private void LowerLevel_Click(object sender, EventArgs e)
        {
            if (Character.Data.Level > 1)
                Character.Data.RemoveLevel();

            RefreshUI();
        }

        private void AdvProficiency_Click(object sender, EventArgs e)
        {
            Character.Data.Proficiencies[ProficiencyList.SelectedIndex].Level++;
            RefreshUI();
        }

        private void DecrProficiency_Click(object sender, EventArgs e)
        {
            Character.Data.Proficiencies[ProficiencyList.SelectedIndex].Level--;
            RefreshUI();
        }

        private void IncrHealthButton_Click(object sender, EventArgs e)
        {
            Character.Data.Health++;

            if (Character.Data.Health > Character.Data.MaxHealth)
                Character.Data.Health = Character.Data.MaxHealth;

            RefreshUI();
        }

        private void DecrHealthButton_Click(object sender, EventArgs e)
        {
            Character.Data.Health--;

            if (Character.Data.Health <= 0)
            {
                Enabled = false;
                DeathForm dForm = new DeathForm();

                dForm.Health = Character.Data.MaxHealth;

                dForm.RefreshUI();

                dForm.ShowDialog();
            }

            RefreshUI();
        }

        public void DeathSuccess()
        {
            Character.Data.Health = 1;
            Enabled = true;
        }

        public void DeathFail()
        {
            Character.Data.Health = 1;
            Enabled = true;
            dead = true;
        }

        private void IncrMaxHealthButton_Click(object sender, EventArgs e)
        {
            Character.Data.MaxHealth++;

            RefreshUI();
        }

        private void DecrMaxHealthButton_Click(object sender, EventArgs e)
        {
            Character.Data.MaxHealth--;

            if (Character.Data.Health > Character.Data.MaxHealth)
                Character.Data.Health = Character.Data.MaxHealth;

            RefreshUI();
        }

        private void DecrProfAdv_Click(object sender, EventArgs e)
        {
            Character.Data.Proficiencies[ProficiencyList.SelectedIndex].Advantage--;

            RefreshUI();
        }

        private void IncrProfAdv_Click(object sender, EventArgs e)
        {
            Character.Data.Proficiencies[ProficiencyList.SelectedIndex].Advantage++;
            RefreshUI();
        }

        private void InventoryList_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (InventoryList.FocusedItem != null && InventoryList.FocusedItem.Group != null)
            {
                if (selectedInventory != null && selectedInventory != InventoryList.FocusedItem)
                {
                    selectedInventory.BackColor = Color.Transparent;
                    selectedInventory.ForeColor = Color.Black;
                }

                selectedInventory = InventoryList.FocusedItem;
                
                if (selectedInventory != null)
                {
                    selectedInventory.BackColor = Color.CornflowerBlue;
                    selectedInventory.ForeColor = Color.White;

                    SelectedItemName.Text = selectedInventory.Text;
                    
                    InventoryItemEquipped.Checked = Character.Data.Inventory.Find(o => o.uid == selectedInventory.Name).Equipped; // o.Name.Equals(selectedInventory.Text) && o.Equipped == (selectedInventory.Group == InventoryList.Groups[0])).Equipped;
                }

                RemoveItem.Enabled = selectedInventory != null;
                SelectedItemName.Enabled = selectedInventory != null;
                InventoryItemEquipped.Enabled = selectedInventory != null;
            }
        }

        private void AddItem_Click(object sender, EventArgs e)
        {
            Character.Data.Inventory.Add(new CharacterData.InventoryObject());
            RefreshInventory();
        }

        private void RemoveItem_Click(object sender, EventArgs e)
        {
            Character.Data.Inventory.Remove(Character.Data.Inventory.Find(o => o.Name.Equals(selectedInventory.Text) && o.Equipped == (selectedInventory.Group == InventoryList.Groups[0])));
            RefreshInventory();

            RemoveItem.Enabled = false;
            SelectedItemName.Enabled = false;
            InventoryItemEquipped.Enabled = false;
        }

        private void RefreshInventory()
        {
            InventoryList.Items.Clear();

            for (int i = 0; i < Character.Data.Inventory.Count; i++)
            {
                ListViewItem newItem = new ListViewItem(Character.Data.Inventory[i].Name, InventoryList.Groups[Character.Data.Inventory[i].Equipped ? 0 : 1]);

                InventoryList.Items.Add(newItem);

                newItem.Name = Character.Data.Inventory[i].uid;
            }

            //try and find the item previously selected
            for(int i = 0; i < InventoryList.Items.Count; i++)
            {
                if(InventoryList.Items[i].Text == SelectedItemName.Text && InventoryList.Items[i].Group == InventoryList.Groups[InventoryItemEquipped.Checked? 0 : 1])
                {
                    selectedInventory = InventoryList.Items[i];

                    selectedInventory.BackColor = Color.CornflowerBlue;
                    selectedInventory.ForeColor = Color.White;
                    break;
                }
            }
            
            RemoveItem.Enabled = selectedInventory != null;
            SelectedItemName.Enabled = selectedInventory != null;
            InventoryItemEquipped.Enabled = selectedInventory != null;
        }

        private void InventoryItemEquipped_CheckedChanged(object sender, EventArgs e)
        {
            Character.Data.Inventory.Find(o => o.uid == selectedInventory.Name).Equipped = InventoryItemEquipped.Checked;// o.Name.Equals(selectedInventory.Text) && o.Equipped == (selectedInventory.Group == InventoryList.Groups[0])).Equipped = InventoryItemEquipped.Checked;
            RefreshInventory();
        }

        private void SelectedItemName_TextChanged(object sender, EventArgs e)
        {
            CharacterData.InventoryObject inv = Character.Data.Inventory.Find(o => o.uid == selectedInventory.Name);// o.Name.Equals(selectedInventory.Text) && o.Equipped == (selectedInventory.Group == InventoryList.Groups[0]));

            inv.Name = SelectedItemName.Text;
            selectedInventory.Text = SelectedItemName.Text;

            RefreshInventory();
        }

        //arcane

        private void ArcaneList_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ArcaneList.FocusedItem != null && ArcaneList.FocusedItem.Group != null)
            {
                if (selectedArcane != null && selectedArcane != ArcaneList.FocusedItem)
                {
                    selectedArcane.BackColor = Color.Transparent;
                    selectedArcane.ForeColor = Color.Black;
                }

                selectedArcane = ArcaneList.FocusedItem;

                if (selectedArcane != null)
                {
                    selectedArcane.BackColor = Color.CornflowerBlue;
                    selectedArcane.ForeColor = Color.White;

                    SpellName.Text = selectedArcane.Text;
                    SpellLevel.Text = (ArcaneList.Groups.IndexOf(selectedArcane.Group) + 1).ToString();
                }

                DeleteSpell.Enabled = selectedArcane != null;
                SpellName.Enabled = selectedArcane != null;
                SpellLevelIncrease.Enabled = selectedArcane != null && (ArcaneList.Groups.IndexOf(selectedArcane.Group) + 1) < 6;
                SpellLevelDecrease.Enabled = selectedArcane != null && (ArcaneList.Groups.IndexOf(selectedArcane.Group) + 1) > 1;
            }
        }

        private void SpellName_TextChanged(object sender, EventArgs e)
        {
            CharacterData.Spell spell = Character.Data.ArcaneSpells.Find(o => o.uid == selectedArcane.Name);// => o.Name.Equals(selectedArcane.Text) && o.Level == (ArcaneList.Groups.IndexOf(selectedArcane.Group) + 1));

            spell.Name = SpellName.Text;
            selectedArcane.Text = SpellName.Text;

            RefreshSpells();
        }
        
        private void NewSpell_Click(object sender, EventArgs e)
        {
            Character.Data.ArcaneSpells.Add(new CharacterData.Spell());
            RefreshSpells();
        }
    
        private void DeleteSpell_Click(object sender, EventArgs e)
        {
            Character.Data.ArcaneSpells.Remove(Character.Data.ArcaneSpells.Find(o => o.uid == selectedArcane.Name));// o.Name.Equals(selectedArcane.Text) && o.Level == (ArcaneList.Groups.IndexOf(selectedArcane.Group) + 1)));
            RefreshSpells();

            DeleteSpell.Enabled = false;
            SpellName.Enabled = false;
            SpellLevelIncrease.Enabled = false;
            SpellLevelDecrease.Enabled = false;
        }

        private void SpellLevelIncrease_Click(object sender, EventArgs e)
        {
            Character.Data.ArcaneSpells.Find(o=> o.uid == selectedArcane.Name).Level++;// => o.Name.Equals(selectedArcane.Text) && o.Level == (ArcaneList.Groups.IndexOf(selectedArcane.Group) + 1)).Level++;
            RefreshSpells();

            SpellLevel.Text = (ArcaneList.Groups.IndexOf(selectedArcane.Group) + 1).ToString();
        }

        private void SpellLevelDecrease_Click(object sender, EventArgs e)
        {
            Character.Data.ArcaneSpells.Find(o=> o.uid == selectedArcane.Name).Level--;// => o.Name.Equals(selectedArcane.Text) && o.Level == (ArcaneList.Groups.IndexOf(selectedArcane.Group) + 1)).Level--;
            RefreshSpells();

            SpellLevel.Text = (ArcaneList.Groups.IndexOf(selectedArcane.Group) + 1).ToString();
        }

        private void RefreshSpells()
        {
            ArcaneList.Items.Clear();

            for (int i = 0; i < Character.Data.ArcaneSpells.Count; i++)
            {
                ListViewItem newspell = new ListViewItem(Character.Data.ArcaneSpells[i].Name, ArcaneList.Groups[Character.Data.ArcaneSpells[i].Level - 1]);
                ArcaneList.Items.Add(newspell);

                newspell.Name = Character.Data.ArcaneSpells[i].uid;
            }

            //try and find the item previously selected
            for (int i = 0; i < ArcaneList.Items.Count; i++)
            {
                if (ArcaneList.Items[i].Text == SpellName.Text && ArcaneList.Items[i].Group == ArcaneList.Groups[Character.Data.ArcaneSpells[i].Level - 1])
                {
                    selectedArcane = ArcaneList.Items[i];

                    selectedArcane.BackColor = Color.CornflowerBlue;
                    selectedArcane.ForeColor = Color.White;
                    break;
                }
            }

            DeleteSpell.Enabled = selectedArcane != null;
            SpellName.Enabled = selectedArcane != null;
            SpellLevelIncrease.Enabled = selectedArcane != null && (ArcaneList.Groups.IndexOf(selectedArcane.Group) + 1) < 6;
            SpellLevelDecrease.Enabled = selectedArcane != null && (ArcaneList.Groups.IndexOf(selectedArcane.Group) + 1) > 1;
        }
      
        private void raceBonus_CheckedChanged(object sender, EventArgs e)
        {
            if (refreshProfDropDownOverride)
            {
                Character.Data.Proficiencies[ProficiencyList.SelectedIndex].RaceBonus = raceBonus.Checked;

                int pointAddition = Character.Data.PointsRemaining > 0 ? 1 : 0;

                Character.Data.Proficiencies[ProficiencyList.SelectedIndex].Level += (raceBonus.Checked? (Character.Data.Proficiencies[ProficiencyList.SelectedIndex].Level > 0? -1 : 0) : pointAddition);
                
                PointsAvailableLabel.Text = Character.Data.PointsRemaining.ToString();

                RefreshProficiencies();

                ProficiencyList.DataSource = null;
                ProficiencyList.DataSource = Character.Data.Proficiencies;
            }
        }
    }
}
