﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CharacterManager66
{
    public partial class TextInputDialog : Form
    {
        public TextInputDialog()
        {
            InitializeComponent();
        }

        public void SetTitle(string title)
        {
            Text = title;
        }

        public void SetText(string text)
        {
            Label.Text = text;
        }
        
        private void DoneButton_Click(object sender, EventArgs e)
        {
            Form1.Instance.AddNewProficiency(textBox1.Text);
            Close();
        }
    }
}
