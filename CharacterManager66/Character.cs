﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CharacterManager66
{
    public partial class Character
    {
        public CharacterData Data = new CharacterData();
        private string filePath = "";

        public string FileName
        {
            get
            {
                if(filePath.Length == 0)
                {
                    return Data.Name + ".66";
                }

                string[] argSplit = Regex.Split(filePath, @"\\");
                string name = argSplit[argSplit.Length - 1];

                return name;
            }
        }

        public void LoadFromArgs()
        {
            string[] args = Environment.GetCommandLineArgs();

            for (int i = 0; i < args.Length; i++)
            {
                Console.WriteLine("args[{0}] == {1}", i, args[i]);
            }

            Console.WriteLine("USING: [" + (args.Length - 1) + "]" + args[args.Length - 1]);

            string filePath = args[args.Length - 1];

            bool isCorrect = ExtensionIsCorrect(filePath);

            if(isCorrect)
                LoadFromFile(filePath);
        }

        public bool ExtensionIsCorrect(string filePath)
        {
            string[] argSplit = Regex.Split(filePath, @"\.");
            string argExtension = argSplit[argSplit.Length - 1];

            return argExtension.Equals("66");
        }

        public void LoadFromFile(string path)
        {
            try
            {
                filePath = path;

                Data = DataSaver.LoadFile<CharacterData>(path);
            }
            catch(Exception e)
            {
                MessageBox.Show("The character file could not be opened.", "Invalid File");
            }
        }

        public bool HasFilePath()
        {
            return filePath.Length > 0;
        }

        public void Save()
        {
            if (filePath.Length == 0)
            {
                filePath = Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + @"\" + Data.Name;
            }

            SaveAs(filePath);
        }

        public void SaveAs(string path)
        {
            bool addPath = !ExtensionIsCorrect(path);

            if(addPath)
            {
                path += ".66";
            }

            MessageBox.Show("Saved as: " + path, "Saved Successfully");

            filePath = path;

            Data.SaveAsFile(path);
        }
    }
}
