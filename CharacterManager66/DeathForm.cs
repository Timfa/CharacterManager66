﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CharacterManager66
{
    public partial class DeathForm : Form
    {
        private Random random = new Random();
        private int DeathPool = 6;
        public int Health = 1;

        public DeathForm()
        {
            InitializeComponent();
            RefreshUI();
            
            CheckTime.Visible = true;
            CheckHitsResult.Visible = true;
        }

        public void RefreshUI()
        {
            HealthRemaining.Text = "Health Remaining: " + Health.ToString();

            DeathPoolSize.Text = DeathPool.ToString();

            DecrDeathPool.Enabled = DeathPool > 0;
            IncrDeathPool.Enabled = DeathPool < 6;
        }

        private void IncrDeathPool_Click(object sender, EventArgs e)
        {
            DeathPool++;

            RefreshUI();
        }

        private void DecrDeathPool_Click(object sender, EventArgs e)
        {
            DeathPool--;

            RefreshUI();

            if (DeathPool <= 0)
            {
                MessageBox.Show("Your character survives.", "Success");
                Form1.Instance.DeathSuccess();
                Close();
            }
        }

        private void DeathRollBtn_Click(object sender, EventArgs e)
        {
            Roll(false, 0, 0, 0, 0, 0, 0);

            DecrDeathPool.Enabled = false;
            IncrDeathPool.Enabled = false;
            DeathRollBtn.Enabled = false;
        }

        private void DeathForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (e.CloseReason == CloseReason.UserClosing && Health > 0 && DeathPool > 0)
            {
                DialogResult confirmResult = MessageBox.Show("Are you sure you want to cancel the Death Rolls?", "Cancel?", MessageBoxButtons.YesNo);

                if (confirmResult == DialogResult.Yes)
                {
                    Form1.Instance.DeathSuccess();
                }

                e.Cancel = confirmResult == DialogResult.No;
            }
        }

        private void Roll(bool reroll, int od1, int od2, int od3, int od4, int od5, int od6)
        {
            int level = DeathPool;

            D2.Visible = level > 1;
            D3.Visible = level > 2;
            D4.Visible = level > 3;
            D5.Visible = level > 4;
            D6.Visible = level > 5;

            Thread roller = new Thread(() =>
            {

                int d1 = reroll ? od1 : random.Next(1, 7);
                int d2 = reroll ? od2 : random.Next(1, 7);
                int d3 = reroll ? od3 : random.Next(1, 7);
                int d4 = reroll ? od4 : random.Next(1, 7);
                int d5 = reroll ? od5 : random.Next(1, 7);
                int d6 = reroll ? od6 : random.Next(1, 7);

                if (reroll)
                {
                    bool done = false;

                    if (d1 == 6 && !done)
                    {
                        d1 = 7;
                        done = true;
                    }

                    if (d2 == 6 && !done)
                    {
                        d2 = 7;
                        done = true;
                    }

                    if (d3 == 6 && !done)
                    {
                        d3 = 7;
                        done = true;
                    }

                    if (d4 == 6 && !done)
                    {
                        d4 = 7;
                        done = true;
                    }

                    if (d5 == 6 && !done)
                    {
                        d5 = 7;
                        done = true;
                    }

                    if (d6 == 6 && !done)
                    {
                        d6 = 7;
                        done = true;
                    }
                }

                int maxVal = 20;
                int minVal = 10;

                bool found = false;
                bool reroll1 = false;
                bool reroll2 = false;
                bool reroll3 = false;
                bool reroll4 = false;
                bool reroll5 = false;
                bool reroll6 = false;

                if (reroll)
                {
                    if (od1 < 4 && !found)
                    {
                        found = true;
                        reroll1 = true;
                        d1 = random.Next(1, 7);
                    }

                    if (od2 < 4 && !found)
                    {
                        found = true;
                        reroll2 = true;
                        d2 = random.Next(1, 7);
                    }

                    if (od3 < 4 && !found)
                    {
                        found = true;
                        reroll3 = true;
                        d3 = random.Next(1, 7);
                    }

                    if (od4 < 4 && !found)
                    {
                        found = true;
                        reroll4 = true;
                        d4 = random.Next(1, 7);
                    }

                    if (od5 < 4 && !found)
                    {
                        found = true;
                        reroll5 = true;
                        d5 = random.Next(1, 7);
                    }

                    if (od6 < 4 && !found)
                    {
                        found = true;
                        reroll6 = true;
                        d6 = random.Next(1, 7);
                    }
                }

                int d1r = !reroll1 && reroll ? od1 : minVal + random.Next(maxVal);
                int d2r = !reroll2 && reroll ? od2 : minVal + random.Next(maxVal);
                int d3r = !reroll3 && reroll ? od3 : minVal + random.Next(maxVal);
                int d4r = !reroll4 && reroll ? od4 : minVal + random.Next(maxVal);
                int d5r = !reroll5 && reroll ? od5 : minVal + random.Next(maxVal);
                int d6r = !reroll6 && reroll ? od6 : minVal + random.Next(maxVal);

                double p = 1;

                Stopwatch timer = new Stopwatch();

                double dT = 0;

                while (p > 0)
                {
                    timer.Start();

                    p -= dT * 0.01;

                    if (!reroll || reroll1)
                        SetDice(D1, (Lerp(d1, d1r, p * p) % 6) + 1, level == 0);

                    if (!reroll || reroll2)
                        SetDice(D2, (Lerp(d2, d2r, p * p) % 6) + 1, level == 0);

                    if (!reroll || reroll3)
                        SetDice(D3, (Lerp(d3, d3r, p * p) % 6) + 1, level == 0);

                    if (!reroll || reroll4)
                        SetDice(D4, (Lerp(d4, d4r, p * p) % 6) + 1, level == 0);

                    if (!reroll || reroll5)
                        SetDice(D5, (Lerp(d5, d5r, p * p) % 6) + 1, level == 0);

                    if (!reroll || reroll6)
                        SetDice(D6, (Lerp(d6, d6r, p * p) % 6) + 1, level == 0);

                    timer.Stop();
                    dT = (double)timer.ElapsedMilliseconds * 0.001;
                }

                SetDice(D1, d1, level == 0);
                SetDice(D2, d2, level == 0);
                SetDice(D3, d3, level == 0);
                SetDice(D4, d4, level == 0);
                SetDice(D5, d5, level == 0);
                SetDice(D6, d6, level == 0);

                int sixes = 0;
                int fails = 0;

                if (d1 == 6)
                    sixes++;

                if (d2 == 6 && level > 1)
                    sixes++;

                if (d3 == 6 && level > 2)
                    sixes++;

                if (d4 == 6 && level > 3)
                    sixes++;

                if (d5 == 6 && level > 4)
                    sixes++;

                if (d6 == 6 && level > 5)
                    sixes++;

                if (d1 <= 3)
                    fails++;

                if (d2 <= 3 && level > 1)
                    fails++;

                if (d3 <= 3 && level > 2)
                    fails++;

                if (d4 <= 3 && level > 3)
                    fails++;

                if (d5 <= 3 && level > 4)
                    fails++;

                if (d6 <= 3 && level > 5)
                    fails++;

                if (sixes > 0 && fails > 0)
                {
                    Thread.Sleep(1000);

                    DeathPoolSize.Invoke((MethodInvoker)delegate
                    {
                        Roll(true, d1, d2, d3, d4, d5, d6);
                    });
                }
                else
                {
                    DeathPoolSize.Invoke((MethodInvoker)delegate
                    {
                        string text = DateTime.Now.ToString("HH:mm");
                        
                        CheckTime.Visible = true;
                        CheckProficiencyUsed.Visible = true;
                        CheckHitsResult.Visible = true;

                        CheckTime.Text = text;
                        
                        DeathRollBtn.Enabled = true;
                        IncrDeathPool.Enabled = true;
                        DecrDeathPool.Enabled = true;
                        
                        int hits = 0;

                        if (d1 > 3)
                            hits++;

                        if (d2 > 3 && level > 1)
                            hits++;

                        if (d3 > 3 && level > 2)
                            hits++;

                        if (d4 > 3 && level > 3)
                            hits++;

                        if (d5 > 3 && level > 4)
                            hits++;

                        if (d6 > 3 && level > 5)
                            hits++;
                        
                        if (d1 == 6) SetDice(D1, 7, false);
                        if (d2 == 6) SetDice(D2, 7, false);
                        if (d3 == 6) SetDice(D3, 7, false);
                        if (d4 == 6) SetDice(D4, 7, false);
                        if (d5 == 6) SetDice(D5, 7, false);
                        if (d6 == 6) SetDice(D6, 7, false);

                        CheckHitsResult.Text = "Rolled " + hits + " Hits, " + fails + " Fails. -" + fails + " Health.";

                        Health -= fails;

                        if(Health <= 0)
                        {
                            MessageBox.Show("Your character has died.", "Failure");
                            
                            Form1.Instance.DeathFail();
                            Close();
                            Form1.Instance.Close();
                        }

                        Bitmap bmp = new Bitmap(CheckResult.Width, CheckResult.Height);

                        CheckResult.DrawToBitmap(bmp, new Rectangle(Point.Empty, CheckResult.Size));

                        Clipboard.SetImage(bmp);

                        CheckTime.Text += " (copied to clipboard)";
                        RefreshUI();
                    });
                }
            });

            roller.Start();
        }

        private int Lerp(int start, int to, double t)
        {
            return (int)Math.Round((double)start + (((double)to - (double)start) * t));
        }

        private void SetDice(PictureBox D, int d, bool fourIsBad)
        {
            if (d == 1)
                D.Image = CharacterManager66.Properties.Resources.dice_1;

            if (d == 2)
                D.Image = CharacterManager66.Properties.Resources.dice_2;

            if (d == 3)
                D.Image = CharacterManager66.Properties.Resources.dice_3;

            if (d == 4)
            {
                D.Image = CharacterManager66.Properties.Resources.dice_4_g;

                if (fourIsBad)
                    D.Image = CharacterManager66.Properties.Resources.dice_4_b;
            }

            if (d == 5)
                D.Image = CharacterManager66.Properties.Resources.dice_5;

            if (d == 6)
                D.Image = CharacterManager66.Properties.Resources.dice_6;

            if (d == 7) //re-rolled 6
                D.Image = CharacterManager66.Properties.Resources.dice_6_r;
        }
    }
}
