﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;
using static CharacterManager66.CharacterData;

namespace CharacterManager66
{
    public partial class BioForm : Form
    {
        public BioForm()
        {
            InitializeComponent();

            int alignment = (int)Form1.Character.Data.Alignment;

            AgeBox.Text = Form1.Character.Data.Age;
            MovementBox.Text = Form1.Character.Data.MovementSpeed;
            BackgroundBox.Text = Form1.Character.Data.Background;
            PersonalityTraitsBox.Text = Form1.Character.Data.PersonalityTraits;
            IdealsBox.Text = Form1.Character.Data.PersonalityIdeals;
            FlawsBox.Text = Form1.Character.Data.PersonalityFlaws;
            BioBox.Text = Form1.Character.Data.Bio;

            string[] alignments = Enum.GetNames(typeof(Alignments));

            for(int i = 0; i < alignments.Length; i++)
            {
                alignments[i] = Regex.Replace(alignments[i], "_", " ");
            }

            AlignmentBox.DataSource = alignments;

            AlignmentBox.SelectedIndex = alignment;
        }

        private void AgeBox_TextChanged(object sender, EventArgs e)
        {
            Form1.Character.Data.Age = AgeBox.Text;
        }

        private void MovementBox_TextChanged(object sender, EventArgs e)
        {
            Form1.Character.Data.MovementSpeed = MovementBox.Text;
        }

        private void AlignmentBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            Form1.Character.Data.Alignment = (Alignments)AlignmentBox.SelectedIndex;
        }

        private void BackgroundBox_TextChanged(object sender, EventArgs e)
        {
            Form1.Character.Data.Background = BackgroundBox.Text;
        }

        private void PersonalityTraitsBox_TextChanged(object sender, EventArgs e)
        {
            Form1.Character.Data.PersonalityTraits = PersonalityTraitsBox.Text;
        }

        private void IdealsBox_TextChanged(object sender, EventArgs e)
        {
            Form1.Character.Data.PersonalityIdeals = IdealsBox.Text;
        }

        private void FlawsBox_TextChanged(object sender, EventArgs e)
        {
            Form1.Character.Data.PersonalityFlaws = FlawsBox.Text;
        }

        private void BioBox_TextChanged(object sender, EventArgs e)
        {
            Form1.Character.Data.Bio = BioBox.Text;
        }
    }
}
