﻿using System;
using System.Collections.Generic;

namespace CharacterManager66
{
    public class CharacterData
    {
        public string Name = "Character";
        public string Race = "Human";
        public List<Proficiency> Proficiencies = new List<Proficiency>();

        public Alignments Alignment = Alignments.True_Neutral;
        public string Age = "0";
        public string MovementSpeed = "10m";

        public string PersonalityTraits = "";
        public string PersonalityIdeals = "";
        public string PersonalityFlaws = "";
        public string Bio = "";

        public string Background = "";

        public List<InventoryObject> Inventory = new List<InventoryObject>();
        public List<Spell> ArcaneSpells = new List<Spell>();

        public int Level = 1;
        private int experience = 0;

        public int Health = 1;
        public int MaxHealth = 1;

        public CharacterData()
        {
            Proficiency unskilled = new Proficiency();
            unskilled.Name = "Unskilled";

            Proficiencies.Add(unskilled);
        }
        
        public int Experience
        {
            get
            {
                return experience;
            }

            set
            {
                experience = value;

                if(experience > Level)
                {
                    experience -= Level + 1;
                    AddLevel();
                }

                if(experience < 0)
                {
                    RemoveLevel();
                    experience = Level;
                }
            }
        }

        public int PointsRemaining
        {
            get
            {
                int totalPoints = 0;

                for(int i = 0; i < Proficiencies.Count; i++)
                {
                    totalPoints += Proficiencies[i].Level;
                }

                int available = (Level + 10) - totalPoints;

                return available;
            }
        }

        public void AddLevel()
        {
            Level++;
            MaxHealth++;
            Health++;
        }

        public void RemoveLevel()
        {
            Level--;

            MaxHealth--;

            if (Health > MaxHealth)
                Health = MaxHealth;
        }

        public class Proficiency
        {
            public int Level = 1;
            public string Name = "New Proficiency";

            public int Advantage = 0;

            public bool RaceBonus = false;

            public int Difficulty
            {
                get
                {
                    return (int)Math.Ceiling((double)(Level + (RaceBonus? 1 : 0)) * 0.5);
                }
            }

            public override string ToString()
            {
                return Name + " - " + (Level + (RaceBonus? 1 : 0)) + (RaceBonus? "*" : "");
            }
        }

        public class InventoryObject
        {
            public InventoryObject()
            {
                uid = Base64Encode(Count.ToString());
                Count++;
            }

            public static int Count = 0;

            public string uid = "inv";
            public string Name = "New Item";
            public bool Equipped = false;
        }

        public class Spell
        {
            public Spell()
            {
                uid = Base64Encode(Count.ToString());
                Count++;
            }

            public static int Count = 0;

            public string uid = "inv";
            public string Name = "New Spell";
            public int Level = 1;
        }

        public static string Base64Encode(string plainText)
        {
            var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(plainText);
            return System.Convert.ToBase64String(plainTextBytes);
        }

        public enum Alignments
        {
            Chaotic_Good,
            Chaotic_Neutral,
            Chaotic_Evil,
            Neutral_Good,
            True_Neutral,
            Neutral_Evil,
            Lawful_Good,
            Lawful_Neutral,
            Lawful_Evil
        }
    }
}
