﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web.Script.Serialization;

namespace CharacterManager66
{
    static class DataSaver
    {
        public static bool FileExists(string fileName)
        {
            return File.Exists(fileName);
        }

        public static void DeleteFile(string fileName)
        {
            if (File.Exists(fileName))
            {
                File.Delete(fileName);
            }
        }

        public static void SaveAsFile<T>(this T data, string fileName)
        {
            Console.WriteLine("Saving File: " + fileName);

            JavaScriptSerializer serializer = new JavaScriptSerializer();

            string jsonData = serializer.Serialize(data);
            
            StreamWriter writer = File.CreateText(fileName);
            writer.Write(jsonData);
            writer.Close();
        }

        public static T LoadFile<T>(string fileName)
        {
            Console.WriteLine("Loading File: " + fileName);

            if (File.Exists(fileName))
            {
                StreamReader reader = File.OpenText(fileName);
                string json = reader.ReadToEnd();

                reader.Close();

                JavaScriptSerializer serializer = new JavaScriptSerializer();

                return serializer.Deserialize<T>(json);
            }

            return default(T);
        }
    }
}
